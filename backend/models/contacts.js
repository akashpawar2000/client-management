const { Model, DataTypes } = require("sequelize");
const sequelize = require("../utils/database.js");

class Contacts extends Model {
  static associate(models) {
    Contacts.belongsTo(models.Address, {
      foreignKey: "AddressNumber",
      targetKey: "id",
      as: "address",
    });
  }
}

Contacts.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    salutation: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    contactPersonName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    designation: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    department: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    officialMobile: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    personalMobile: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    linkdinProfile: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    typeOfBusiness: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    AddressNumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    remarks: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    preferredModeOfCommunication: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "contacts",
    modelName: "Contacts",
    timestamps: false,
  }
);

module.exports = Contacts;
