const { Model, DataTypes } = require("sequelize");
const sequelize = require("../utils/database.js");

class Company extends Model {
  static associate(models) {
    Company.hasMany(models.Address, {
      as: "addresses",
      foreignKey: "companyNumber",
      sourceKey: "companyNumber",
    });
  }
}

Company.init(
  {
    companyNumber: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    nameOfCompany: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    companyWebsite: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    linkdinProfile: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    sequelize,
    timestamps: false,
    modelName: "Company",
    tableName: "company",
  }
);

module.exports = Company;
