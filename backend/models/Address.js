const { Model, DataTypes } = require("sequelize");
const sequelize = require("../utils/database.js");

class Address extends Model {
  static associate(models) {
    Address.belongsTo(models.Company, {
      foreignKey: "companyNumber",
      targetKey: "companyNumber",
      as: "company",
    });

    Address.hasMany(models.Contacts, {
      as: "contacts",
      foreignKey: "AddressNumber",
      sourceKey: "id",
    });
  }
}

Address.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    companyAddress: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    state: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pincode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    officeTelephone: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fieldOfActivity: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    companyNumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    sequelize,
    timestamps: false,
    modelName: "Address",
    tableName: "address",
  }
);

module.exports = Address;
