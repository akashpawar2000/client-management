require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("./utils/database.js");
const apiRoutes = require("./routes/api.js");
const app = express();
const cors = require("cors");
const Company = require("./models/company.js");
const Address = require("./models/Address.js");
const Contacts = require("./models/contacts.js");

app.use(bodyParser.json());
app.use(cors());
app.use("/api", apiRoutes);

const models = {
  Company: Company,
  Address: Address,
  Contacts: Contacts,
};

Company.associate(models);
Address.associate(models);
Contacts.associate(models);

const port = 8000;

sequelize
  .sync({ force: false })
  .then(() => {
    console.log("Database & tables synced");
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  })
  .catch((error) => {
    console.error("Error syncing database:", error);
  });
