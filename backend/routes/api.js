const express = require("express");
const router = express.Router();
const GetAllForms = require("../controller/getAllForms.js");
const AddForm = require("../controller/addForm.js");
const UpdateForm = require("../controller/updateForm.js");
const onlySearchByCompanyName = require("../controller/onlySearchByCompanyName.js");
const searchCompany = require("../controller/searchCompany.js");
const sendEMail = require("../controller/Email/sendEmail.js");

//to get all forms
router.get("/getallforms", GetAllForms.getAllForms);

router.get("/getbyNumber/:companyNumber", GetAllForms.getByCompanyNumber);

//to add new form
router.post("/addform", AddForm.createForm);

//to update form
router.post("/updateform", UpdateForm.updateFormByCompanyNumber);

//sending company name object to frontend
router.get("/onlycompany", onlySearchByCompanyName.getAllCompanyNames);

//to search companny in search field for frontend
router.get("/search", searchCompany.searchCompany);

//send email to client
router.post("/send-mail", sendEMail.sendEmailToClient);

module.exports = router;
