const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("companyform", "root", "12345", {
  host: "localhost",
  port: "3307",
  dialect: "mysql",
  logging: console.log,
  logging: (msg) => console.log(`[Sequelize]: ${msg}`),
});

module.exports = sequelize;
