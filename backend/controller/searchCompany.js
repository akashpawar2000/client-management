const { Op } = require("sequelize");
const sequelize = require("../utils/database.js");
const Company = require("../models/company.js");
const Address = require("../models/Address.js");
const Contacts = require("../models/contacts.js");

const searchCompany = async (req, res) => {
  const search = req.query.search || "";

  console.log("Query Parameters:", { search });

  try {
    let whereCondition = {};

    if (search) {
      whereCondition = {
        [Op.or]: [
          sequelize.where(
            sequelize.fn("lower", sequelize.col("Company.nameOfCompany")),
            { [Op.like]: `%${search.toLowerCase()}%` }
          ),
        ],
      };
    }

    console.log("Where Condition:", whereCondition);

    const records = await Company.findAll({
      where: whereCondition,
      include: [
        {
          model: Address,
          as: "addresses",
          include: [{ model: Contacts, as: "contacts" }],
        },
      ],
    });

    console.log("Records:", records);

    res.status(200).json({
      data: records,
    });
  } catch (error) {
    console.error("Error fetching data from database: ", error);
    res.status(500).json({ error: "An error occurred while fetching data" });
  }
};

module.exports = {
  searchCompany,
};
