const { text } = require("body-parser");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
  },
});

const sendEmailToClient = async (req, res) => {
  try {
    const { to, subject, body } = req.body;
    
    const mailOptions = {
      from: process.env.EMAIL_USER,
      to: to,
      subject: subject,
      text: body,
    };

    transporter.sendMail(mailOptions, async (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
        if (res) return res.status(500).json({ error: "Error sending email" });
        throw error;
      }
      console.log("Email sent:", info.response);

      if (res)
        res.status(200).json({ message: "Email sent successfully and logged" });
    });
  } catch (error) {
    console.error("Unexpected error:", error);
    if (res) res.status(500).json({ error: "An unexpected error occurred" });
    else throw error;
  }
};

module.exports = {
  sendEmailToClient,
};
