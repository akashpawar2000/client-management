const Company = require("../models/company.js");
const Address = require("../models/Address.js");
const Contacts = require("../models/contacts.js");

const createForm = async (req, res) => {
  try {
    const {
      nameOfCompany,
      addresses,
      companyWebsite,
      category,
      linkdinProfile,
    } = req.body;

    const createdCompany = await Company.create({
      nameOfCompany,
      companyWebsite,
      category,
      linkdinProfile,
    });

    if (addresses && Array.isArray(addresses)) {
      const createdAddresses = await Promise.all(
        addresses.map(async (addressInfo) => {
          const createdAddress = await Address.create({
            ...addressInfo,
            companyNumber: createdCompany.companyNumber,
          });

          if (addressInfo.contacts && Array.isArray(addressInfo.contacts)) {
            const createdContacts = await Promise.all(
              addressInfo.contacts.map(async (contactInfo) => {
                const createdContact = await Contacts.create({
                  ...contactInfo,
                  AddressNumber: createdAddress.id,
                });
                return createdContact;
              })
            );
            console.log(
              "Created Contacts for Address ",
              createdAddress.id,
              ": ",
              createdContacts
            );
          }

          return createdAddress;
        })
      );
      console.log("Created Addresses: ", createdAddresses);
    }

    res.status(200).json({
      message: "Form data saved successfully",
      company: createdCompany,
    });
  } catch (error) {
    console.error("Error saving company data:", error);
    res
      .status(500)
      .json({ message: "Failed to save company data", error: error.message });
  }
};

module.exports = {
  createForm,
};
