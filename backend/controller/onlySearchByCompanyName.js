const sequelize = require("../utils/database.js");
const Company = require("../models/company.js");

const getAllCompanyNames = async (req, res) => {
  try {
    const companies = await Company.findAll({
      attributes: ['nameOfCompany'],
    });

    const companyNames = companies.map(company => company.nameOfCompany);

    res.status(200).json({
      data: companyNames,
    });
    
  } catch (error) {
    console.error("Error fetching company names from database: ", error);
    res.status(500).json({ error: "An error occurred while fetching company names" });
  }
};

module.exports = {
  getAllCompanyNames,
};
