const Company = require("../models/company.js");
const Address = require("../models/Address.js");
const Contacts = require("../models/contacts.js");

const getAllForms = async (req, res) => {
  try {
    const forms = await Company.findAll({
      include: [
        {
          model: Address,
          as: "addresses",
          include: [
            {
              model: Contacts,
              as: "contacts"
            }
          ]
        }
      ]
    });
    res.status(200).json({ data: forms });
  } catch (error) {
    console.error("Error fetching data from database: ", error);
    res.status(500).json({ error: "An error occurred while fetching data" });
  }
};

const getByCompanyNumber = async (req, res) => {
  const companyNumber = req.params.companyNumber;
  try {
    const company = await Company.findOne({
      where: { companyNumber },
      include: [
        {
          model: Address,
          as: "addresses",
          include: [
            {
              model: Contacts,
              as: "contacts"
            }
          ]
        }
      ]
    });

    if (!company) {
      return res.status(404).json({ error: `Company with companyNumber ${companyNumber} not found` });
    }

    res.status(200).json({ data: company });
  } catch (error) {
    console.error(`Error fetching company with companyNumber ${companyNumber}: `, error);
    res.status(500).json({ error: "An error occurred while fetching company data" });
  }
};

module.exports = {
  getAllForms,
  getByCompanyNumber,
};
