const Company = require("../models/company.js");
const Address = require("../models/Address.js");
const Contacts = require("../models/contacts.js");
const sequelize = require("../utils/database.js");

const updateFormByCompanyNumber = async (req, res) => {
  const companyNumber = req.body.companyNumber;
  const { nameOfCompany, addresses, companyWebsite, linkdinProfile, category } =
    req.body;

  const transaction = await sequelize.transaction(); // Start a transaction

  try {
    let company = await Company.findOne({
      where: { companyNumber: companyNumber },
      transaction,
    });

    if (!company) {
      await transaction.rollback();
      return res.status(404).json({ message: "Company not found" });
    }

    const currentAddresses = await Address.findAll({
      where: { companyNumber: company.companyNumber },
      include: [{ model: Contacts, as: "contacts" }],
      transaction,
    });

    // Identify addresses and contacts to be deleted
    const currentAddressIds = currentAddresses.map((addr) => addr.id);
    const newAddressIds = addresses
      .filter((addr) => addr.id)
      .map((addr) => addr.id);
    const deletedAddresses = currentAddressIds.filter(
      (id) => !newAddressIds.includes(id)
    );

    const currentContactIds = currentAddresses.flatMap((addr) =>
      addr.contacts.map((contact) => contact.id)
    );
    const newContactIds = addresses
      .flatMap((addr) => addr.contacts || [])
      .filter((contact) => contact.id)
      .map((contact) => contact.id);
    const deletedContacts = currentContactIds.filter(
      (id) => !newContactIds.includes(id)
    );

    // Update company information
    company = await company.update(
      {
        nameOfCompany,
        companyWebsite,
        linkdinProfile,
        category,
      },
      { transaction }
    );

    if (addresses && Array.isArray(addresses)) {
      await Promise.all(
        addresses.map(async (addressInfo) => {
          let address;
          if (addressInfo.id) {
            address = await Address.findByPk(addressInfo.id, { transaction });
            if (address) {
              await address.update(
                {
                  ...addressInfo,
                  companyNumber: company.companyNumber,
                },
                { transaction }
              );
            }
          } else {
            address = await Address.create(
              {
                ...addressInfo,
                companyNumber: company.companyNumber,
              },
              { transaction }
            );
          }

          if (addressInfo.contacts && Array.isArray(addressInfo.contacts)) {
            await Promise.all(
              addressInfo.contacts.map(async (contactInfo) => {
                if (contactInfo.id) {
                  const existingContact = await Contacts.findByPk(
                    contactInfo.id,
                    { transaction }
                  );
                  if (existingContact) {
                    await existingContact.update(
                      {
                        ...contactInfo,
                        AddressNumber: address.id,
                      },
                      { transaction }
                    );
                  }
                } else {
                  await Contacts.create(
                    {
                      ...contactInfo,
                      AddressNumber: address.id,
                    },
                    { transaction }
                  );
                }
              })
            );
          }
        })
      );
    }

    // Delete removed addresses and contacts
    if (deletedAddresses.length > 0) {
      await Promise.all(
        deletedAddresses.map(async (addressId) => {
          const address = await Address.findByPk(addressId, { transaction });
          if (address) {
            await Contacts.destroy({
              where: { AddressNumber: addressId },
              transaction,
            });
            await address.destroy({ transaction });
          }
        })
      );
    }

    if (deletedContacts.length > 0) {
      await Promise.all(
        deletedContacts.map(async (contactId) => {
          const contact = await Contacts.findByPk(contactId, { transaction });
          if (contact) {
            await contact.destroy({ transaction });
          }
        })
      );
    }

    const updatedCompany = await Company.findOne({
      where: { companyNumber: company.companyNumber },
      include: [
        {
          model: Address,
          as: "addresses",
          include: [
            {
              model: Contacts,
              as: "contacts",
            },
          ],
        },
      ],
      transaction,
    });

    await transaction.commit();

    res.status(200).json({
      message: "Company updated successfully",
      company: updatedCompany,
    });
  } catch (error) {
    await transaction.rollback();
    console.error("Error updating company", error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = {
  updateFormByCompanyNumber,
};
