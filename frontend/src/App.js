// App.js

import React from "react";
import LoginContext from "./context/loginContext";
import { useContext } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

const App = () => {
  const { isAuthenticated, setIsAuthenticated } = useContext(LoginContext);
  const handleLogout = () => {
    localStorage.removeItem("authenticated");

    setIsAuthenticated(false);
  };
  return (
    <>
      <div>
        <div className="header bg-blue-500 text-white flex items-center justify-between px-4 py-2">
          <div className="flex items-center">
            <div className="image w-12 h-12 bg-gray-400 rounded-full overflow-hidden mr-4">
              <img
                src="rkd.jpg"
                alt="RKD Logo"
                style={{ width: "100%", height: "100%", objectFit: "cover" }}
              />
            </div>
            <div className="websiteName flex-grow text-lg font-bold flex items-center">
              <span className="mx-auto">Client Details Management</span>
            </div>
          </div>
          <div className="flex items-center">
            <div className="date mr-4">{`${new Date().getDate()}/0${
              new Date().getMonth() + 1
            }/${new Date().getFullYear()}`}</div>
            <button
              className="text-white bg-blue-500 hover:bg-blue-700 py-1 px-4 rounded focus:outline-none focus:shadow-outline"
              onClick={handleLogout}
            >
              Log out
            </button>
          </div>
        </div>
  
      </div>
    </>
  );
};

export default App;
