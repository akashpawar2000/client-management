import { useContext, useState } from "react";
import LoginContext from "./context/loginContext";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { isAuthenticated, setIsAuthenticated } = useContext(LoginContext);
  const history = useHistory();
  const handleLogin = () => {
    fetch("http://192.168.1.191:5000/api/login", {
      method: "POST", // Corrected method name to uppercase
      headers: {
        "Content-Type": "application/json", // Corrected content type header
      },
      body: JSON.stringify({ username: username, password: password }), // Assuming data is your login credentials object
    })
      .then((response) => {
        if (response.ok) {
          setIsAuthenticated(true);
          localStorage.setItem("authenticated", true);
          alert("Login successful");
          history.push("/");
        } else {
          alert("login failed");
        }
      })
      .catch((error) => {
        // Handle error
      });
  };
  return (
    <div className="relative flex flex-col justify-center items-center h-screen">
      {/* Animated background image */}
      <div className="absolute inset-0 overflow-hidden z-0">
        {/* Image tag for your company image */}
        <img
          className="absolute inset-0 w-full h-full object-cover z-0 filter  opacity-100 animate-fade-in"
          src="rkd.jpg"
          alt="Company Image"
        />
        <div className="absolute inset-0 bg-gradient-to-b from-transparent to-gray-900 opacity-60 z-10"></div>
      </div>

      {/* Login form */}
      <div className="bg-white shadow-md rounded-md p-8 max-w-md w-full z-10 bg-opacity-50 transition-opacity opacity-100 translate-y-0">
        <h1 className="text-3xl font-bold mb-8 text-center text-blue-600">
          R. K. Dewan & Co.
        </h1>
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="username"
          >
            Username
          </label>
          <input
            className="input-field w-full"
            id="username"
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="password"
          >
            Password
          </label>
          <input
            className="input-field w-full"
            id="password"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline w-full"
          type="button"
          onClick={handleLogin}
        >
          Sign In
        </button>
      </div>
    </div>
  );
}

export default Login;
