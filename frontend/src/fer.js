import React, { useState, useEffect } from "react";
import * as XLSX from "xlsx";
const moment = require("moment");

function FER() {
  const [patent, setPatent] = useState({
    APPLICATION_NUMBER: "",
    APPLICATION_TYPE: "",
    APPLICANT_TYPE: "",
    JURISDICTION: "",
    APPLICANT_NAME: "",
    Group: "",
    DATE_OF_FILING: "",
    TITLE_OF_INVENTION: "",
    FER_Issue_Date: "",
    DUE_DATE_FOR_FER_Reply_Date: "",
    UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
    FER_Reply_Date: "",
    IPO_Application_Status: "",
    STATUS: "",
    mailLogs2: [], //mail
    ferRemarks: [], //association
    CLIENT_RESPONSE: "",
    EXAMINER: "",
    ASSOCIATE_NAME: "",
    INFORM_CLIENT_DUE_DATE: "",
    CLIENT_INFORMED_DATE: "",
    NO_OF_EMAILS_SENT_TO_CLIENT: "",
    CLIENT_INSTRUCTION_RECIEVED_DATE: "",
    ALLOTMENT_DATE: "",
    ASSOCIATE_DEADLINE: "",
    Date: "",
    CLIENT_APPROVAL_DATE: "",
    REQUEST_FOR_EXTENSION_FILED_DATE: "",
    FIRST_DRAFT_SUBMISSION_DATE: "",
    CLIENT_RESPONSE: "",
    applicants: [],
    inventors: [],
    CLIENT_NAME: "",
    Client_Email_ID_To: "",
    Client_Email_ID_CC: "",
    Client_Email_ID_BCC: "",
    Client_Email_ID_From: "",
    NO_OF_CITED_DOCUMENTS: "",
    FORMAL_REQUIREMENT_INTIMATION_DATE: "",
    REVIEWER_MAIL_STC_DATE: "",
    Client_Instruction_for_Non_Compliance_of_Formals: "",
    REVIEWER_COMMENTS: "",
    LATEST_CLIENT_MAIL_DATE: "",
    FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
    LATEST_REMINDER_SENT_DATE: "",
    PETITION_UNDER_RULE_138_FILED_DATE: "",
    APPLICATION_WITHDRAWN_DATE: "",
    INVOICE_NUMBER: "",
    INVOICE_GENERATED_DATE: "",
    Special_Client_or_Regular_Client: "",
    CLIENT_APPROVAL_DETAILS: "",
    FORMAL_OBJECTION_REASONS: "",
    STANDING_INSTRUCTIONS_BY_CLIENT: "",
    file: [],
    ALERT_STATUS: "",
    Controller_Name: "",
    System_Mail_Log: "",
  });
  const [applicant, setApplicant] = useState({ APPLICANT_NAME: "" });
  const [searchName, setSearchName] = useState("");
  const [clients, setClients] = useState([]);
  const [inventor, setInventor] = useState({ INVENTOR_NAME: "" });
  const [stage, setStage] = useState("");
  const [statusTracker, setStatusTracker] = useState([]);
  const [alertTracker, setAlertTracker] = useState([]);
  const [patents, setPatents] = useState([]);
  const [searchApplicationNo, setSearchApplicationNo] = useState("");
  const [searchStage, setSearchStage] = useState("");
  const [showPopup, setShowPopup] = useState(false);
  const [showStage, setShowStage] = useState(false);
  const [showSearch, setShowSearch] = useState(false);
  const [addPopup, setAddPopup] = useState(false);
  const [updatePopup, setUpdatePopup] = useState(false);
  const [totalApplicants, setTotalApplicants] = useState("");
  const [editable, setEditable] = useState(false);
  const [edit, setEdit] = useState(false);
  const [stageCount, setStageCount] = useState([]);
  // State to track the value of the input
  const [inputValue, setInputValue] = useState("");
  const [currentPopup, setCurrentPopup] = useState(null);
  const [popup3Active, setPopup3Active] = useState(false);
  const [popup4Active, setPopup4Active] = useState(false);
  const [to, setTo] = useState("");
  const [cc, setCC] = useState("");
  const [bcc, setBCC] = useState("");
  const [from, setFrom] = useState("");
  const [subject, setSubject] = useState("");
  const [text, setText] = useState("");
  const [message, setMessage] = useState("");
  const [selectedValue, setSelectedValue] = useState("");
  const [filterTo, setFilterTo] = useState("");
  const [filterStage, setFilterStage] = useState("");
  const [filterFrom, setFilterFrom] = useState("");
  const [ferIssueDate, setFerIssueDate] = useState("");
  const [clientInformedDate, setClientInformedDate] = useState("");
  const [clientInformDueDate, setClientInformDueDate] = useState("");
  const [reminderSent, setReminderSent] = useState(false);
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };

  const handleEditButton = () => {
    setEdit(true);
  };
  // Function to handle button click
  const handleButtonClick = () => {
    // Toggle the editable state
    setEditable(true);
  };

  const [searchAll, setSearchAll] = useState("");
  //applicant and inventor state

  useEffect(() => {
    fetch(`http://192.168.1.191:5000/api/totalpatent`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        console.log(data.count);
        setTotalApplicants(data.count);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  }, [patent]);

  const openPopup = () => {
    setShowPopup(true);
  };

  const closePopup = () => {
    setShowPopup(false);
  };
  const closeStagePopup = () => {
    setShowStage(false);
    setSearchStage("");
    setStage("");
  };
  const addSixMonths = (dateString) => {
    if (dateString !== undefined && !isNaN(Date.parse(dateString))) {
      const date = new Date(dateString);
      date.setMonth(date.getMonth() + 6);
      return date.toISOString().slice(0, 10);
    }
    return null; // or any other appropriate value for invalid input
  };

  const limitEighteenMonths = (dateString) => {
    if (dateString !== undefined && !isNaN(Date.parse(dateString))) {
      const date = new Date(dateString);
      date.setMonth(date.getMonth() + 18);
      return date.toISOString().slice(0, 10);
    }
    return null; // or any other appropriate value for invalid input
  };

  useEffect(() => {
    if (patent.FER_Issue_Date != undefined) {
      setPatent((prevPatent) => ({
        ...prevPatent,

        DUE_DATE_FOR_FER_Reply_Date: addSixMonths(prevPatent.FER_Issue_Date),
      }));
    }
  }, [patent.FER_Issue_Date]);

  const limitDueDate = (e) => {
    const selectedDate = e.target.value;
    patent.UPDATED_DUE_DATE_FOR_FER_Reply_Date = new Date(
      patent.DUE_DATE_FOR_FER_Reply_Date
    );
    if (new Date(selectedDate) > addSixMonths) {
      alert(
        "Date should be within 18 months from the initial Due Date of FER Reply"
      );
      return;
    }
  };

  const closeSearchPopup = () => {
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      JURISDICTION: "",
      APPLICANT_NAME: "",
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      IPO_Application_Status: "",
      STATUS: "",
      mailLogs2: [], //mail
      ferRemarks: [], //association
      CLIENT_RESPONSE: "",
      EXAMINER: "",
      ASSOCIATE_NAME: "",
      INFORM_CLIENT_DUE_DATE: "",
      CLIENT_INFORMED_DATE: "",
      NO_OF_EMAILS_SENT_TO_CLIENT: "",
      CLIENT_INSTRUCTION_RECIEVED_DATE: "",
      ALLOTMENT_DATE: "",
      ASSOCIATE_DEADLINE: "",
      Date: "",
      CLIENT_APPROVAL_DATE: "",
      REQUEST_FOR_EXTENSION_FILED_DATE: "",
      FIRST_DRAFT_SUBMISSION_DATE: "",
      CLIENT_RESPONSE: "",
      applicants: [],
      inventors: [],
      CLIENT_NAME: "",
      Client_Email_ID_To: "",
      Client_Email_ID_CC: "",
      Client_Email_ID_BCC: "",
      Client_Email_ID_From: "",
      NO_OF_CITED_DOCUMENTS: "",
      FORMAL_REQUIREMENT_INTIMATION_DATE: "",
      REVIEWER_MAIL_STC_DATE: "",
      Client_Instruction_for_Non_Compliance_of_Formals: "",
      REVIEWER_COMMENTS: "",
      LATEST_CLIENT_MAIL_DATE: "",
      FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
      LATEST_REMINDER_SENT_DATE: "",
      PETITION_UNDER_RULE_138_FILED_DATE: "",
      APPLICATION_WITHDRAWN_DATE: "",
      INVOICE_NUMBER: "",
      INVOICE_GENERATED_DATE: "",
      Special_Client_or_Regular_Client: "",
      CLIENT_APPROVAL_DETAILS: "",
      FORMAL_OBJECTION_REASONS: "",
      STANDING_INSTRUCTIONS_BY_CLIENT: "",
      file: [],
      ALERT_STATUS: "",
      Controller_Name: "",
      System_Mail_Log: "",
    });
    setSearchApplicationNo("");
    setShowSearch(false);
    setEditable(false);
  };

  const handleSave = () => {
    // Here you can handle saving the data
    // if(setSearchApplicationNo.toString().trim().length !== 12)
    // {

    // }
    handleSubmit();
  };

  const [mailLog, setmailLog] = useState({
    Mail: "",
    Date_1: "",
    response: "",
    Date_2: "",
  });
  const [remark, setRemark] = useState({ Date: "", Remark: "" });
  const addMail = () => {
    console.log("mailLog", mailLog.response);

    setPatent((prev) => ({
      ...prev,
      mailLogs2: [...prev.mailLogs2, { ...mailLog }], // Create a new object for the new remark
    }));
    setmailLog({
      Mail: "",
      Date_1: "",
      response: "",
      Date_2: "",
    });
  };
  const addRemark = () => {
    console.log(patent);
    if (remark.Remark) {
      setPatent((prev) => ({
        ...prev,
        ferRemarks: [
          ...prev.ferRemarks,
          { ...remark, Date: new Date().toISOString().slice(0, 10) },
        ], // Create a new object for the new remark
      }));
      setRemark({ Date: "", Remark: "" });
    }
    // setRemark((prevRemark) => ({
    //   ...prevRemark,

    //   Date: new Date().toISOString().slice(0, 10), // Get YYYY-MM-DD string
    // }));
  };
  // useEffect(() => {
  //   if (remark.Date && remark.Remark) {
  //     setPatent(
  //       (prev) => ({
  //         ...prev,
  //         remarks: [...prev.remarks, { ...remark }], // Create a new object for the new remark
  //       }),

  //     );
  //   }
  // }, [remark]);

  const patentStatus = {
    [1]: "Fer issued and Client to be informed",
    [2]: "Client informed and Client instruction awaiitng",
    [3]: "Client instruction received",
    [4]: "Allotted to Associate",
    [5]: "Pending with Associate",
    [6]: "First draft received ",
    [7]: "Final draft received ",
    [8]: "FER Reply Filed",
  };
  useEffect(() => {
    setStatusTracker([
      patent.FER_Issue_Date,
      patent.CLIENT_INFORMED_DATE,
      patent.CLIENT_INSTRUCTION_RECIEVED_DATE,
      patent.ALLOTMENT_DATE,
      patent.ASSOCIATE_DEADLINE,
      patent.FIRST_DRAFT_SUBMISSION_DATE,
      patent.CLIENT_APPROVAL_DATE,
      patent.FER_Reply_Date,
    ]);
  }, [
    patent.FER_Issue_Date,
    patent.CLIENT_INFORMED_DATE,
    patent.CLIENT_INSTRUCTION_RECIEVED_DATE,
    patent.ALLOTMENT_DATE,
    patent.ASSOCIATE_DEADLINE,
    patent.FIRST_DRAFT_SUBMISSION_DATE,
    patent.CLIENT_APPROVAL_DATE,
    patent.FER_Reply_Date,
  ]);
  useEffect(() => {
    for (let i = 0; i < statusTracker.length; i++) {
      if (statusTracker[i] != "") {
        if (statusTracker[i] == "Pending") continue;
        setPatent((prevPatent) => ({
          ...prevPatent,
          STATUS: patentStatus[i + 1],
        }));
        break;
      }
    }
  }, [statusTracker]);

  const alertStatus = {
    [1]: "Client Not Reported",
    [2]: "Remind Client for Instruction for FER reply",
    [3]: "FER reply Not-Alloted to Associate",
    [4]: "FER-reply draft Not Submitted by Associate",
    [5]: "FER Reply draft Not STC",
    [6]: "Remind Client for Approval ",
    [7]: "Remind to File FER reply ",
  };
  useEffect(() => {
    setAlertTracker([
      patent.INFORM_CLIENT_DUE_DATE,
      patent.LATEST_CLIENT_MAIL_DATE,
      patent.CLIENT_INSTRUCTION_RECIEVED_DATE,
      patent.ASSOCIATE_DEADLINE,
      patent.FIRST_DRAFT_SUBMISSION_DATE,
      patent.LATEST_REMINDER_SENT_DATE,
      patent.CLIENT_APPROVAL_DATE,
    ]);
  }, [
    patent.INFORM_CLIENT_DUE_DATE,
    patent.LATEST_CLIENT_MAIL_DATE,
    patent.CLIENT_INSTRUCTION_RECIEVED_DATE,
    patent.ASSOCIATE_DEADLINE,
    patent.FIRST_DRAFT_SUBMISSION_DATE,
    patent.LATEST_REMINDER_SENT_DATE,
    patent.CLIENT_APPROVAL_DATE,
  ]);
  useEffect(() => {
    for (let i = 0; i < alertTracker.length; i++) {
      if (alertTracker[i] != "") {
        if (alertTracker[i] == "FER issued/FER Reply not filed") continue;
        setPatent((prevPatent) => ({
          ...prevPatent,
          ALERT_STATUS: alertStatus[i + 1],
        }));
        break;
      }
    }
  }, [alertTracker]);

  const handleSearchButton = () => {
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      JURISDICTION: "",
      APPLICANT_NAME: "",
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      IPO_Application_Status: "",
      STATUS: "",
      mailLogs2: [], //mail
      ferRemarks: [], //association
      CLIENT_RESPONSE: "",
      EXAMINER: "",
      ASSOCIATE_NAME: "",
      INFORM_CLIENT_DUE_DATE: "",
      CLIENT_INFORMED_DATE: "",
      NO_OF_EMAILS_SENT_TO_CLIENT: "",
      CLIENT_INSTRUCTION_RECIEVED_DATE: "",
      ALLOTMENT_DATE: "",
      ASSOCIATE_DEADLINE: "",
      Date: "",
      CLIENT_APPROVAL_DATE: "",
      REQUEST_FOR_EXTENSION_FILED_DATE: "",
      FIRST_DRAFT_SUBMISSION_DATE: "",
      CLIENT_RESPONSE: "",
      applicants: [],
      inventors: [],
      CLIENT_NAME: "",
      Client_Email_ID_To: "",
      Client_Email_ID_CC: "",
      Client_Email_ID_BCC: "",
      Client_Email_ID_From: "",
      NO_OF_CITED_DOCUMENTS: "",
      FORMAL_REQUIREMENT_INTIMATION_DATE: "",
      REVIEWER_MAIL_STC_DATE: "",
      Client_Instruction_for_Non_Compliance_of_Formals: "",
      REVIEWER_COMMENTS: "",
      LATEST_CLIENT_MAIL_DATE: "",
      FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
      LATEST_REMINDER_SENT_DATE: "",
      PETITION_UNDER_RULE_138_FILED_DATE: "",
      APPLICATION_WITHDRAWN_DATE: "",
      INVOICE_NUMBER: "",
      INVOICE_GENERATED_DATE: "",
      Special_Client_or_Regular_Client: "",
      CLIENT_APPROVAL_DETAILS: "",
      FORMAL_OBJECTION_REASONS: "",
      STANDING_INSTRUCTIONS_BY_CLIENT: "",
      file: [],
      ALERT_STATUS: "",
      Controller_Name: "",
      System_Mail_Log: "",
    });
    setShowSearch(true);
  };

  const handleSearch = () => {
    if (searchApplicationNo.trim() === "") {
      alert("Application No cant't be empty");
    } else {
      console.log("application_no", patent.APPLICATION_NUMBER);
      fetch(
        `http://192.168.1.191:5000/api/getFer?APPLICATION_NUMBER=${searchApplicationNo}`
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network Error");
          }
          console.log(response);
          return response.json();
        })
        .then((data) => {
          console.log("Object from backend", data.application);
          console.log("dp=dsfsfta", data.application.file);
          console.log("dp=dsfsfta1111", data.application.files);

          setSearchApplicationNo("");
          setPatent(data.application);
          handleEditButton();
        })
        .catch((error) => {
          console.error("Fetch error:", error);
          alert(
            "Application not found, Please Enter correct Application Number"
          );
          setPatent({
            APPLICATION_NUMBER: "",
            APPLICATION_TYPE: "",
            JURISDICTION: "",
            APPLICANT_NAME: "",
            Group: "",
            DATE_OF_FILING: "",
            TITLE_OF_INVENTION: "",
            DUE_DATE_OF_PUBLICATION: "",
            PUBLICATION_DATE_U_S_11A: "",
            REQUEST_FOR_EXAMINATION_DATE: "",
            FER_Issue_Date: "",
            DUE_DATE_FOR_FER_Reply_Date: "",
            UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
            FER_Reply_Date: "",
            IPO_Application_Status: "",
            STATUS: "",
            mailLogs2: [], //mail
            ferRemarks: [], //association
            CLIENT_RESPONSE: "",
            EXAMINER: "",
            ASSOCIATE_NAME: "",
            INFORM_CLIENT_DUE_DATE: "",
            CLIENT_INFORMED_DATE: "",
            NO_OF_EMAILS_SENT_TO_CLIENT: "",
            CLIENT_INSTRUCTION_RECIEVED_DATE: "",
            ALLOTMENT_DATE: "",
            ASSOCIATE_DEADLINE: "",
            Date: "",
            CLIENT_APPROVAL_DATE: "",
            REQUEST_FOR_EXTENSION_FILED_DATE: "",
            FIRST_DRAFT_SUBMISSION_DATE: "",
            CLIENT_RESPONSE: "",
            applicants: [],
            inventors: [],
            CLIENT_NAME: "",
            Client_Email_ID_To: "",
            Client_Email_ID_CC: "",
            Client_Email_ID_BCC: "",
            Client_Email_ID_From: "",
            NO_OF_CITED_DOCUMENTS: "",
            FORMAL_REQUIREMENT_INTIMATION_DATE: "",
            REVIEWER_MAIL_STC_DATE: "",
            Client_Instruction_for_Non_Compliance_of_Formals: "",
            REVIEWER_COMMENTS: "",
            LATEST_CLIENT_MAIL_DATE: "",
            FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
            LATEST_REMINDER_SENT_DATE: "",
            PETITION_UNDER_RULE_138_FILED_DATE: "",
            APPLICATION_WITHDRAWN_DATE: "",
            INVOICE_NUMBER: "",
            INVOICE_GENERATED_DATE: "",
            Special_Client_or_Regular_Client: "",
            CLIENT_APPROVAL_DETAILS: "",
            FORMAL_OBJECTION_REASONS: "",
            STANDING_INSTRUCTIONS_BY_CLIENT: "",
            file: [],
            ALERT_STATUS: "",
            Controller_Name: "",
            System_Mail_Log: "",
          });
        });
    }
  };
  useEffect(() => {
    fetch(`http://192.168.1.191:5000/api/totalpatents`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        const clients = data.flatMap(({ applicants }) =>
          applicants.flatMap(({ APPLICANT_NAME }) => APPLICANT_NAME)
        );

        setClients([...new Set(clients)]);
        console.log(clients);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  }, []);

  const handleTotalSearch = () => {
    console.log(searchStage);
    setSearchStage("");
    console.log("in search function");
    fetch(`http://192.168.1.191:5000/api/totalpatent`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        console.log(data);
        console.log(data.count);
        setEditable(false);
        setPatents(data);
        setStage(patents.length);
        setShowStage(true);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  };

  const handleSearchStage = () => {
    console.log(searchStage);
    console.log("in search function");
    fetch(
      `http://192.168.1.191:5000/api/patents? =${encodeURIComponent(
        searchStage
      )}`
    )
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        console.log(data);
        console.log(data.count);
        setEditable(false);
        setPatents(data);
        setStage(patents.length);
        setShowStage(true);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
        setPatents([]);
      });
  };

  const handleSearchStatus = async () => {
    if (searchStage) {
      fetch(
        `http://192.168.1.191:5000/api/statuspatent?Stage=${encodeURIComponent(
          searchStage
        )}`
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network Error");
          }
          console.log(response);
          return response.json();
        })
        .then((data) => {
          setEditable(false);
          setPatents(data);
          setStage(patents.length);
          setShowStage(true);
        })
        .catch((error) => {
          setPatents([]);
          setShowStage(true);
          console.error("Fetch error:", error);
        });
    }
  };

  const handleSearchApplicantName = async (name) => {
    console.log(name);

    if (name) {
      fetch(
        `http://192.168.1.191:5000/api/applicantName?APPLICANT_NAME=${encodeURIComponent(
          name
        )}`
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network Error");
          }
          console.log(response);
          return response.json();
        })
        .then((data) => {
          setPatents(data);
          setShowStage(true);
          displayPopup("popup3");
        })
        .catch((error) => {
          setPatents([]);
          setShowStage(true);
          console.error("Fetch error:", error);
        });
      setSearchName(name);
    }
  };
  //this is for opdate through seacrh application no popup
  const handleSubmit = async () => {
    console.log("patent", patent);
    console.log("My final patent object", patent);
    try {
      const response = await fetch("http://192.168.1.191:5000/api/ferUpdate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(patent),
      });

      if (!response.ok) {
        throw new Error("Failed to submit patent data");
      }
      setPatent({
        APPLICATION_NUMBER: "",
        APPLICATION_TYPE: "",
        JURISDICTION: "",
        APPLICANT_NAME: "",
        Group: "",
        DATE_OF_FILING: "",
        TITLE_OF_INVENTION: "",
        DUE_DATE_OF_PUBLICATION: "",
        PUBLICATION_DATE_U_S_11A: "",
        REQUEST_FOR_EXAMINATION_DATE: "",
        FER_Issue_Date: "",
        DUE_DATE_FOR_FER_Reply_Date: "",
        UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
        FER_Reply_Date: "",
        IPO_Application_Status: "",
        STATUS: "",
        mailLogs2: [], //mail
        ferRemarks: [], //association
        CLIENT_RESPONSE: "",
        EXAMINER: "",
        ASSOCIATE_NAME: "",
        INFORM_CLIENT_DUE_DATE: "",
        CLIENT_INFORMED_DATE: "",
        NO_OF_EMAILS_SENT_TO_CLIENT: "",
        CLIENT_INSTRUCTION_RECIEVED_DATE: "",
        ALLOTMENT_DATE: "",
        ASSOCIATE_DEADLINE: "",
        Date: "",
        CLIENT_APPROVAL_DATE: "",
        REQUEST_FOR_EXTENSION_FILED_DATE: "",
        FIRST_DRAFT_SUBMISSION_DATE: "",
        CLIENT_RESPONSE: "",
        applicants: [],
        inventors: [],
        CLIENT_NAME: "",
        Client_Email_ID_To: "",
        Client_Email_ID_CC: "",
        Client_Email_ID_BCC: "",
        Client_Email_ID_From: "",
        NO_OF_CITED_DOCUMENTS: "",
        FORMAL_REQUIREMENT_INTIMATION_DATE: "",
        REVIEWER_MAIL_STC_DATE: "",
        Client_Instruction_for_Non_Compliance_of_Formals: "",
        REVIEWER_COMMENTS: "",
        LATEST_CLIENT_MAIL_DATE: "",
        FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
        LATEST_REMINDER_SENT_DATE: "",
        PETITION_UNDER_RULE_138_FILED_DATE: "",
        APPLICATION_WITHDRAWN_DATE: "",
        INVOICE_NUMBER: "",
        INVOICE_GENERATED_DATE: "",
        Special_Client_or_Regular_Client: "",
        CLIENT_APPROVAL_DETAILS: "",
        FORMAL_OBJECTION_REASONS: "",
        STANDING_INSTRUCTIONS_BY_CLIENT: "",
        file: [],
        ALERT_STATUS: "",
        Controller_Name: "",
        System_Mail_Log: "",
      });
      setEditable(false);
      alert("Data Updated Sucessfully");
    } catch (error) {
      // Handle errors
      console.error("Error submitting patent data:", error.message);
    }
  };

  const updateApplication = async () => {
    console.log("patent", patent);
    try {
      const response = await fetch("http://192.168.1.191:5000/api/ferUpdate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(patent),
      });

      const responseData = await response.json();

      if (response.status === 200) {
        // Data inserted successfully
        alert("Data updated Successfully");
        if (searchStage) {
          handleSearchStatus();
        } else {
          console.log("hey");
          handleTotalSearch();
        }
      } else if (response.status === 400) {
        // Application number already exists
        alert(responseData.error);
      } else {
        // Other error cases
        throw new Error("An error occurred while submitting patent data");
      }
      setPatent({
        APPLICATION_NUMBER: "",
        APPLICATION_TYPE: "",
        JURISDICTION: "",
        APPLICANT_NAME: "",
        Group: "",
        DATE_OF_FILING: "",
        TITLE_OF_INVENTION: "",
        DUE_DATE_OF_PUBLICATION: "",
        PUBLICATION_DATE_U_S_11A: "",
        REQUEST_FOR_EXAMINATION_DATE: "",
        FER_Issue_Date: "",
        DUE_DATE_FOR_FER_Reply_Date: "",
        UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
        FER_Reply_Date: "",
        IPO_Application_Status: "",
        STATUS: "",
        mailLogs2: [], //mail
        ferRemarks: [], //association
        CLIENT_RESPONSE: "",
        EXAMINER: "",
        ASSOCIATE_NAME: "",
        INFORM_CLIENT_DUE_DATE: "",
        CLIENT_INFORMED_DATE: "",
        NO_OF_EMAILS_SENT_TO_CLIENT: "",
        CLIENT_INSTRUCTION_RECIEVED_DATE: "",
        ALLOTMENT_DATE: "",
        ASSOCIATE_DEADLINE: "",
        Date: "",
        CLIENT_APPROVAL_DATE: "",
        REQUEST_FOR_EXTENSION_FILED_DATE: "",
        FIRST_DRAFT_SUBMISSION_DATE: "",
        CLIENT_RESPONSE: "",
        applicants: [],
        inventors: [],
        CLIENT_NAME: "",
        Client_Email_ID_To: "",
        Client_Email_ID_CC: "",
        Client_Email_ID_BCC: "",
        Client_Email_ID_From: "",
        NO_OF_CITED_DOCUMENTS: "",
        FORMAL_REQUIREMENT_INTIMATION_DATE: "",
        REVIEWER_MAIL_STC_DATE: "",
        Client_Instruction_for_Non_Compliance_of_Formals: "",
        REVIEWER_COMMENTS: "",
        LATEST_CLIENT_MAIL_DATE: "",
        FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
        LATEST_REMINDER_SENT_DATE: "",
        PETITION_UNDER_RULE_138_FILED_DATE: "",
        APPLICATION_WITHDRAWN_DATE: "",
        INVOICE_NUMBER: "",
        INVOICE_GENERATED_DATE: "",
        Special_Client_or_Regular_Client: "",
        CLIENT_APPROVAL_DETAILS: "",
        FORMAL_OBJECTION_REASONS: "",
        STANDING_INSTRUCTIONS_BY_CLIENT: "",
        file: [],
        ALERT_STATUS: "",
        Controller_Name: "",
        System_Mail_Log: "",
      });
      setEditable(false);
      alert("Data Updated Sucessfully");
    } catch (error) {
      // Handle errors
      console.error("Error submitting patent data:", error.message);
    }

    // else {
    //   alert(
    //     `${patent.RKD_Remarks.trim() === "" ? "RKD Remarks" : ""} ${
    //       patent.APPLICATION_NUMBER.trim().length !== 12 ? "Applicant No" : ""
    //     } can't be empty`
    //   );
    // }
  };
  const showUpdate = (index) => {
    console.log(index);
    console.log("patent", patents[index]);
    setPatent((prev) => ({
      ...prev,
      ...patents[index],
    }));
    setUpdatePopup(true);
  };

  const showAdd = () => {
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      JURISDICTION: "",
      APPLICANT_NAME: "",
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      IPO_Application_Status: "",
      STATUS: "",
      mailLogs2: [], //mail
      ferRemarks: [], //association
      CLIENT_RESPONSE: "",
      EXAMINER: "",
      ASSOCIATE_NAME: "",
      INFORM_CLIENT_DUE_DATE: "",
      CLIENT_INFORMED_DATE: "",
      NO_OF_EMAILS_SENT_TO_CLIENT: "",
      CLIENT_INSTRUCTION_RECIEVED_DATE: "",
      ALLOTMENT_DATE: "",
      ASSOCIATE_DEADLINE: "",
      Date: "",
      CLIENT_APPROVAL_DATE: "",
      REQUEST_FOR_EXTENSION_FILED_DATE: "",
      FIRST_DRAFT_SUBMISSION_DATE: "",
      CLIENT_RESPONSE: "",
      applicants: [],
      inventors: [],
      CLIENT_NAME: "",
      Client_Email_ID_To: "",
      Client_Email_ID_CC: "",
      Client_Email_ID_BCC: "",
      Client_Email_ID_From: "",
      NO_OF_CITED_DOCUMENTS: "",
      FORMAL_REQUIREMENT_INTIMATION_DATE: "",
      REVIEWER_MAIL_STC_DATE: "",
      Client_Instruction_for_Non_Compliance_of_Formals: "",
      REVIEWER_COMMENTS: "",
      LATEST_CLIENT_MAIL_DATE: "",
      FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
      LATEST_REMINDER_SENT_DATE: "",
      PETITION_UNDER_RULE_138_FILED_DATE: "",
      APPLICATION_WITHDRAWN_DATE: "",
      INVOICE_NUMBER: "",
      INVOICE_GENERATED_DATE: "",
      Special_Client_or_Regular_Client: "",
      CLIENT_APPROVAL_DETAILS: "",
      FORMAL_OBJECTION_REASONS: "",
      STANDING_INSTRUCTIONS_BY_CLIENT: "",
      file: [],
      ALERT_STATUS: "",
      Controller_Name: "",
      System_Mail_Log: "",
    });
    setAddPopup(true);
  };
  const closeAddPopup = () => {
    setAddPopup(false);
  };
  const closeUpdatePopup = () => {
    setUpdatePopup(false);
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      JURISDICTION: "",
      APPLICANT_NAME: "",
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      IPO_Application_Status: "",
      STATUS: "",
      mailLogs2: [], //mail
      ferRemarks: [], //association
      CLIENT_RESPONSE: "",
      EXAMINER: "",
      ASSOCIATE_NAME: "",
      INFORM_CLIENT_DUE_DATE: "",
      CLIENT_INFORMED_DATE: "",
      NO_OF_EMAILS_SENT_TO_CLIENT: "",
      CLIENT_INSTRUCTION_RECIEVED_DATE: "",
      ALLOTMENT_DATE: "",
      ASSOCIATE_DEADLINE: "",
      Date: "",
      CLIENT_APPROVAL_DATE: "",
      REQUEST_FOR_EXTENSION_FILED_DATE: "",
      FIRST_DRAFT_SUBMISSION_DATE: "",
      CLIENT_RESPONSE: "",
      applicants: [],
      inventors: [],
      CLIENT_NAME: "",
      Client_Email_ID_To: "",
      Client_Email_ID_CC: "",
      Client_Email_ID_BCC: "",
      Client_Email_ID_From: "",
      NO_OF_CITED_DOCUMENTS: "",
      FORMAL_REQUIREMENT_INTIMATION_DATE: "",
      REVIEWER_MAIL_STC_DATE: "",
      Client_Instruction_for_Non_Compliance_of_Formals: "",
      REVIEWER_COMMENTS: "",
      LATEST_CLIENT_MAIL_DATE: "",
      FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
      LATEST_REMINDER_SENT_DATE: "",
      PETITION_UNDER_RULE_138_FILED_DATE: "",
      APPLICATION_WITHDRAWN_DATE: "",
      INVOICE_NUMBER: "",
      INVOICE_GENERATED_DATE: "",
      Special_Client_or_Regular_Client: "",
      CLIENT_APPROVAL_DETAILS: "",
      FORMAL_OBJECTION_REASONS: "",
      STANDING_INSTRUCTIONS_BY_CLIENT: "",
      file: [],
      ALERT_STATUS: "",
      Controller_Name: "",
      System_Mail_Log: "",
    });

    console.log("search stage:", searchName);
    // handleSearchApplicantName(searchName);
  };

  useEffect(() => {
    fetch("http://192.168.1.191:5000/api/statuscount")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        // Do something with the data received from the server
        console.log("data", data);
        setStageCount((prev) => [...prev, ...data]);
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      });
  }, [patent]);

  const updateData = (e, setter) => {
    const value = e.target.value;
    console.log(e.target.type);
    console.log(value);

    setter(value);
  };

  const updateResponse = (index) => {
    const existingResponse = patent.mailLogs2[index];
  };

  const downloadExcel = () => {
    const ws = XLSX.utils.json_to_sheet(
      patents.map((patent) => ({
        "Application No": patent.APPLICATION_NUMBER || "",
        "Application Type": patent.APPLICATION_TYPE || "",
        "Patent Office": patent.JURISDICTION || "",
        "Applicant Name": (patent.applicants || [])
          .map(({ APPLICANT_NAME }) => APPLICANT_NAME)
          .join("\n"),
        "Inventor Name": (patent.inventors || [])
          .map(({ INVENTOR_NAME }) => INVENTOR_NAME)
          .join("\n"),
        "Title of Invention": patent.TITLE_OF_INVENTION || "",
        Group: patent.Group || "",
        "Date of Filing": patent.DATE_OF_FILING || "",
        "Due Date of Publication": patent.DUE_DATE_OF_PUBLICATION || "",
        "Publication Date": patent.PUBLICATION_DATE_U_S_11A || "",
        "Request for Examination Date":
          patent.REQUEST_FOR_EXAMINATION_DATE || "",
        "FER Issue Date": patent.FER_Issue_Date || "",
        "Due Date for FER reply": patent.DUE_DATE_FOR_FER_Reply_Date || "",
        "FER Reply Date": patent.FER_Reply_Date || "",
        "Hearing Date": patent.Hearing_Date || "",
        "WS Filing Date": patent.WS_filing_date || "",
        "IPO Application Status": patent.IPO_Application_Status || "",
        STATUS: patent.STATUS || "",
        "Granted or Rejected": patent.Granted_or_Rejected || "",
        "Controller Name": patent.Controller_Name || "",
        "Controller Reply": patent.Controller_Reply || "",
        mailLog: (patent.mailLogs2 || [])
          .map(
            ({ Mail, Date_1, response, Date_2 }, index) =>
              `(${index} : ${Mail} - ${Date_1}  || ${response} - ${Date_2})`
          )
          .join("\n"),
        "Client Response": patent.CLIENT_RESPONSE || "",
        ferRemarks: (patent.ferRemarks || [])
          .map(({ Date, Remark }, index) => ` ${index} : ${Date} - ${Remark}`)
          .join("\n"), // Include remarks
        // Add more fields here as needed
      }))
    );

    const opts = {
      dateNF: "dd/mm/yyyy", // Specify the desired date format
      // cellDates: true, // Uncomment this line if your dates are not being displayed correctly
    };

    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Patents");
    XLSX.writeFile(wb, "patents.xlsx", opts);
  };

  const deleteApplicant = (index) => {
    setPatent((prev) => ({
      ...prev,
      applicants: [
        ...prev.applicants.slice(0, index),
        ...prev.applicants.slice(index + 1),
      ],
    }));
  };
  const deleteInventor = (index) => {
    setPatent((prev) => ({
      ...prev,
      inventors: [
        ...prev.inventors.slice(0, index),
        ...prev.inventors.slice(index + 1),
      ],
    }));
  };

  // Function to display a popup by ID
  const displayPopup = (popupId) => {
    setCurrentPopup(popupId);
  };

  // Function to close the currently displayed popup
  const ClosePopup = () => {
    setCurrentPopup(null);
    if (currentPopup === "popup4") {
      setCurrentPopup("popup3");
    } else {
      setCurrentPopup(null);
    }
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      JURISDICTION: "",
      APPLICANT_NAME: "",
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      UPDATED_DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      IPO_Application_Status: "",
      STATUS: "",
      mailLogs2: [], //mail
      ferRemarks: [], //association
      CLIENT_RESPONSE: "",
      EXAMINER: "",
      ASSOCIATE_NAME: "",
      INFORM_CLIENT_DUE_DATE: "",
      CLIENT_INFORMED_DATE: "",
      NO_OF_EMAILS_SENT_TO_CLIENT: "",
      CLIENT_INSTRUCTION_RECIEVED_DATE: "",
      ALLOTMENT_DATE: "",
      ASSOCIATE_DEADLINE: "",
      Date: "",
      CLIENT_APPROVAL_DATE: "",
      REQUEST_FOR_EXTENSION_FILED_DATE: "",
      FIRST_DRAFT_SUBMISSION_DATE: "",
      CLIENT_RESPONSE: "",
      applicants: [],
      inventors: [],
      CLIENT_NAME: "",
      Client_Email_ID_To: "",
      Client_Email_ID_CC: "",
      Client_Email_ID_BCC: "",
      Client_Email_ID_From: "",
      NO_OF_CITED_DOCUMENTS: "",
      FORMAL_REQUIREMENT_INTIMATION_DATE: "",
      REVIEWER_MAIL_STC_DATE: "",
      Client_Instruction_for_Non_Compliance_of_Formals: "",
      REVIEWER_COMMENTS: "",
      LATEST_CLIENT_MAIL_DATE: "",
      FIRST_DRAFT_STC_FOR_REVIEW_DATE: "",
      LATEST_REMINDER_SENT_DATE: "",
      PETITION_UNDER_RULE_138_FILED_DATE: "",
      APPLICATION_WITHDRAWN_DATE: "",
      INVOICE_NUMBER: "",
      INVOICE_GENERATED_DATE: "",
      Special_Client_or_Regular_Client: "",
      CLIENT_APPROVAL_DETAILS: "",
      FORMAL_OBJECTION_REASONS: "",
      STANDING_INSTRUCTIONS_BY_CLIENT: "",
      file: [],
      ALERT_STATUS: "",
      Controller_Name: "",
      System_Mail_Log: "",
    });
  };

  const openPopup4 = () => {
    setPopup3Active(false);
    setPopup4Active(true);
  };

  const closePopup4 = () => {
    setPopup3Active(true);
    setPopup4Active(false);
  };

  const addThreeDays = (dateString) => {
    if (dateString !== undefined && !isNaN(Date.parse(dateString))) {
      const date = new Date(dateString);
      date.setDate(date.getDate() + 3);
      return date.toISOString().slice(0, 10);
    }
    return null; // or any other appropriate value for invalid input
  };

  useEffect(() => {
    if (patent.FER_Issue_Date != undefined) {
      setPatent((prevPatent) => ({
        ...prevPatent,

        INFORM_CLIENT_DUE_DATE: addThreeDays(prevPatent.Date),
      }));
    }
  }, [patent.Date]);

  const handleEmailSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://192.168.1.191:5000/api/send-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          to: to.split(",").map((email) => email.trim()),
          cc: cc.split(",").map((email) => email.trim()),
          bcc: bcc.split(",").map((email) => email.trim()),
          subject,
          text,
        }),
      });
      if (response.ok) {
        setMessage("Email sent successfully");
      } else {
        setMessage("Error sending email. Please try again later.");
      }
    } catch (error) {
      console.error("Error sending email:", error);
      setMessage("Error sending email. Please try again later.");
    }
  };

  const handleFileChange = (e) => {
    for (let i = 0; i < e.target.files.length; i++) {
      let file = e.target.files[i];
      console.log("Type", typeof file);
      const reader = new FileReader();
      console.log("file name", file.name);
      reader.onloadend = () => {
        // Encode file data as base64 before sending to the backend
        const base64Data = reader.result;
        setPatent((prevPatent) => ({
          ...prevPatent,
          file: [...prevPatent.file, { name: file.name, data: base64Data }],
        }));
      };

      reader.readAsDataURL(file);
    }
  };

  const filterData = async () => {
    console.log("in filter data");

    // Assuming handleTotalSearchCopy is an asynchronous function
    if (filterTo && filterFrom && filterStage) {
      console.log("heeeee");
      let filteredData = patents.filter((patent) => {
        return (
          patent[filterStage] != undefined &&
          moment(patent[filterStage]).isBetween(
            moment(filterFrom),
            moment(filterTo)
          )
        );
      });

      console.log("filteredData", filteredData);

      if (filteredData.length > 0) {
        setPatents(filteredData);
      } else {
        setPatents([]);
      }
    } else {
      alert("Enter all filter data");
    }
  };

  const filterDataAll = async () => {
    console.log("in filter data");

    // Assuming handleTotalSearchCopy is an asynchronous function
    if (searchAll) {
      console.log("heeeee");
      let filteredData = patents.filter((patent) => {
        return searchAll != undefined && JSON.stringify(patent).inc;
      });

      console.log("filteredData", filteredData);

      if (filteredData.length > 0) {
        setPatents(filteredData);
      } else {
        setPatents([]);
      }
    } else {
      alert("Enter all filter data");
    }
  };
  const removeFilter = () => {
    console.log("searchName", searchName);
    if (searchName) handleSearchApplicantName(searchName);
    else if (searchStage) handleSearchStatus();
    else handleTotalSearchCopy();
    setFilterFrom("");
    setFilterTo("");
    setFilterStage("");
  };

  const handleTotalSearchCopy = () => {
    console.log(searchStage);
    setSearchStage("");
    console.log("in search function");
    fetch(`http://192.168.1.191:5000/api/totalpatents`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        console.log("hiiiii");
        setPatents(data);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  };

  const handleClientMailDateChange = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      LATEST_CLIENT_MAIL_DATE: value,
    }));
    const currentDate = moment();
    const threeDaysLater = moment(value).add(15, "days");

    if (
      moment(value) &&
      !patent.CLIENT_INSTRUCTION_RECIEVED_DATE &&
      currentDate > threeDaysLater
    ) {
      alert("Remind Client for Instruction");
    }
  };

  const handleAllotment = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      CLIENT_INSTRUCTION_RECIEVED_DATE: value,
    }));
    const currentDate = moment();
    const threeDaysLater = moment(value).add(3, "days");

    if (
      moment(value) &&
      !patent.ALLOTMENT_DATE &&
      currentDate > threeDaysLater
    ) {
      alert("Please allot FER- reply draft to associate");
    }
  };

  const calculateAssociateDeadline = () => {
    const allotmentDate = moment(patent.ALLOTMENT_DATE);
    const deadlineDate = allotmentDate.clone().add(1, "months");
    const threeWeeksReminderDate = allotmentDate.clone().add(3, "weeks");

    return { deadlineDate, threeWeeksReminderDate };
  };

  const sendReminderAlerts = () => {
    const { deadlineDate, threeWeeksReminderDate } =
      calculateAssociateDeadline();
    const currentDate = moment();

    if (
      currentDate >= threeWeeksReminderDate &&
      currentDate < deadlineDate &&
      !patent.FIRST_DRAFT_SUBMISSION_DATE
    ) {
      alert("Please submit reply draft for review.");
    } else if (
      currentDate >= deadlineDate &&
      !patent.FIRST_DRAFT_SUBMISSION_DATE
    ) {
      alert("Reply draft not submitted.");
    }
  };

  const handleAssociateDeadlineChange = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      ALLOTMENT_DATE: value,
    }));
    sendReminderAlerts();
  };

  const handleFirstDraftSubmissionDateChange = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      FIRST_DRAFT_SUBMISSION_DATE: value,
    }));
    sendReminderAlerts();
  };

  const handleSTCNotsent = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      FIRST_DRAFT_SUBMISSION_DATE: value,
    }));

    const currentDate = moment();
    const threeDaysLater = moment(value).add(3, "days");

    if (
      moment(value) &&
      !patent.FIRST_DRAFT_STC_FOR_REVIEW_DATE &&
      currentDate > threeDaysLater
    ) {
      alert("Please STC FER-reply draft for review");
    }
  };

  const handleClientApproval = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      LATEST_REMINDER_SENT_DATE: value,
    }));
    const currentDate = moment();
    const threeDaysLater = moment(value).add(15, "days");

    if (
      moment(value) &&
      !patent.CLIENT_APPROVAL_DATE &&
      currentDate > threeDaysLater
    ) {
      alert("Remind Client for Approval of FER reply draft");
    }
  };

  const handleClientApprovalDateChange = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      CLIENT_APPROVAL_DATE: value,
    }));
    if (
      patent.CLIENT_APPROVAL_DETAILS === "Approval" &&
      !patent.FER_Reply_Date
    ) {
      const currentDate = moment();
      const threeDaysLater = moment(value).add(3, "days");

      if (currentDate > threeDaysLater) {
        alert("Please file FER reply.");
      }
    }
  };

  const handleClientApprovalStatusChange = (e) => {
    const value = e.target.value;
    setPatent((prevPatent) => ({
      ...prevPatent,
      CLIENT_APPROVAL_DETAILS: value,
    }));

    if (value === "Withdrawn" && !patent.APPLICATION_WITHDRAWN_DATE) {
      alert("Please enter application withdrawn date.");
    }

    if (
      value === "Approval" &&
      patent.CLIENT_APPROVAL_DETAILS &&
      !patent.FER_Reply_Date
    ) {
      const currentDate = moment();
      const approvalDate = moment(patent.CLIENT_APPROVAL_DATE);
      const threeDaysLater = approvalDate.clone().add(3, "days");

      if (currentDate > threeDaysLater) {
        alert("Please file FER reply.");
      }
    }
  };

  const Alerts = ({ alerts }) => {
    return (
      <ul>
        {alerts.map((alerts, index) => (
          <li key={index}>{alerts}</li>
        ))}
      </ul>
    );
  };

  const alerts = [
    "Client Not Reported",
    "Remind Client for Instruction for FER reply",
    "FER reply Not-Alloted to Associate",
    "FER-reply draft Not Submitted by Associate",
    "FER Reply draft Not STC",
    "Remind Client for Approval",
    "Remind to File FER reply",
  ];

  return (
    <>
      {/* Header- hright-15%  width and  all the divs on one line image on left websiteName on center and date on right side*/}
      <div class="header bg-blue-500 text-white flex items-center justify-between px-4 py-2">
        <div className="image"></div>
        <div class="websiteName flex-grow text-xl font-bold text-center">
          FER MODULE
        </div>
        <div className="date">
          {`${new Date().getDate()}/0${
            new Date().getMonth() + 1
          }/${new Date().getFullYear()}`}
        </div>
      </div>

      <div class="ferwebpage">
        <div className="staticContent1 flex flex-wrap bg-gray-100 p-6 rounded-lg shadow-md">
          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("FER issued/FER Reply not filed");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              FER issued/FER Reply not filed:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === "FER issued/FER Reply not filed"
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("FER Replied");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              FER Replied:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(({ STATUS }) => STATUS === "FER Replied")
                  ?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Fer Issued & Client to be Informed");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              FER Issued & Client to be Informed:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) =>
                    STATUS === "Fer Issued & Client to be Informed"
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Client Informed & Client Instruction Awaiitng");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Client Informed & Client Instruction Awaiitng:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) =>
                    STATUS === "Client Informed & Client Instruction Awaiitng"
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Client not responding");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Client not responding:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === "Client not responding"
                )?.statusCount
              }
            </span>
          </div>
          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Client instruction received");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Client instruction received:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === "Client instruction received"
                )?.statusCount
              }
            </span>
          </div>
          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Allotted to Associate");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Allotted to Associate:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === "Allotted to Associate"
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Pending with Associate");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Pending with Associate:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === " Pending with Associate"
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("Final draft Received ");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              Final Draft Received :{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(
                  ({ STATUS }) => STATUS === "Final draft Received "
                )?.statusCount
              }
            </span>
          </div>

          <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
            <span
              className="cursor-pointer text-blue-500 font-bold"
              onClick={() => {
                setSearchStage("FER Reply Filed");
                handleSearchStatus();
                displayPopup("popup3");
              }}
            >
              FER Reply Filed:{" "}
            </span>
            <span className="text-lg font-semibold">
              {
                stageCount.find(({ STATUS }) => STATUS === "RFER Reply Filed")
                  ?.statusCount
              }
            </span>
          </div>
        </div>

        {/* static content 2- Height 20% all the divs should be in one line and at the center and divs should have some gap between them */}
        <div className="staticContent2 flex flex-wrap p-4 bg-gray-100 justify-between">
          <div className="section flex items-center mr-4 mb-4">
            <div className="input-group">
              <input
                type="text"
                placeholder="Enter Applicant Name"
                onChange={(e) => updateData(e, (value) => setSearchName(value))}
                className="border border-gray-300 rounded-md px-2 py-2 mr-2"
              ></input>
              <select
                onChange={(e) => handleSearchApplicantName(e.target.value)}
                className="border border-gray-300 rounded-md px-3 py-2 max-w-xs"
              >
                {searchName === ""
                  ? clients.sort().map((name, index) => (
                      <option key={index} value={name}>
                        {name}
                      </option>
                    ))
                  : clients
                      .sort()
                      .filter((name, index) =>
                        name.toLowerCase().includes(searchName.toLowerCase())
                      )
                      .map((name, index) => (
                        <option key={index} value={name}>
                          {name}
                        </option>
                      ))}
              </select>
            </div>
          </div>

          <div className="section flex items-center mr-4 mb-4">
            <div className="input-group">
              <button
                className="button bg-blue-500 text-white px-4 py-2 rounded-md"
                onClick={(handleSearchButton) => displayPopup("popup1")}
              >
                Search Application No
              </button>
            </div>
          </div>

          <div className="section flex items-center mb-3">
            <div className="input-group mr-2">
              <select
                value={searchStage}
                onChange={(e) =>
                  updateData(e, (value) => setAlertTracker(value))
                }
                className="border border-gray-300 rounded-md px-3 py-2"
              >
                {/* Options here */}
                <option value="">Select Alerts </option>
                <option value="Client Not Reported">Client Not Reported</option>
                <option value="Remind Client for Instructions on FER Reply">
                  Remind Client for Instruction for FER reply
                </option>
                <option value="FER reply  NOT-ALLOTED">
                  FER reply Not-Alloted to Associate
                </option>
                <option value="FER-reply draft NOT SUBMITTED">
                  FER-reply draft Not Submitted by Associate
                </option>
                <option value="FER Reply draft Not STC">
                  FER Reply draft Not STC
                </option>
                <option value="Remind Client for Approval">
                  Remind Client for Approval
                </option>
                <option value="Remind to File FER reply">
                  Remind to File FER reply
                </option>
              </select>
              <button
                className="button bg-blue-500 text-white px-4 py-2 rounded-md"
                onClick={(handleSearchStatus) => displayPopup("popup3")}
              >
                Search
              </button>
            </div>
          </div>
        </div>

        <div className="margin"></div>

        {/* i want this in proper tabular format like means it is for viewing resul so user shouldnt get confused, it should be neat, this pop will get open when user will click on search application no button ,this pop up also contains input tags so it should be neat */}
        <div className="dynamicContent overflow-x">
          <div className="searchPopup">
            {currentPopup === "popup1" && (
              <div className="popup">
                {/* <button onClick={ClosePopup}>Close</button> */}
                {/* I want this pop-header in goot color,center and when i scroll, it should be fixed */}

                <div className="popup-content bg-white shadow-md rounded-md p-4">
                  <div className="popup-header relative mb-4">
                    <div className="pop-header text-3xl font-semibold bg-yellow-300 p-4 rounded-lg text-center">
                      Application Search{" "}
                    </div>
                    <span
                      className="close cursor-pointer absolute top-0 right-0 p-3 text-3xl"
                      onClick={ClosePopup}
                    >
                      &times;
                    </span>
                  </div>
                  {/* I want application no,input box ,search and edit in one line */}
                  <div className="popup-search flex-wrap items-center mb-4 justify-around grid grid-cols-2 space-x-10 1rem">
                    <div class=" p-4  flex-1">
                      <label className="mr-2">Application No:</label>
                      <input
                        type="text"
                        limi
                        value={searchApplicationNo}
                        onChange={(e) =>
                          updateData(e, (value) =>
                            setSearchApplicationNo(value)
                          )
                        }
                        className="border rounded-md p-1 mr-2"
                      />
                      <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded"
                        onClick={handleSearch}
                      >
                        Search
                      </button>
                    </div>

                    <div class=" p-4  flex-1">
                      <div class="text-lg font-semibold"> Status </div>
                      <div class="mt-1 w-50% py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {" "}
                        {patent.STATUS}
                      </div>
                    </div>
                  </div>

                  {/* in popup-data there are 21 fields i want first 20 fields side by side means there will be four columns 1.data-label 2.data-data 3.data-label 4.data-data and last field status at bttom with its data big and bold */}
                  <div class="grid grid-cols-4 gap-4">
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        FER Info Enter Date:
                      </div>
                      <div class="mt-1 ">
                        <input
                          type="date"
                          readOnly={!editable}
                          value={patent.Date}
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Client Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent?.CLIENT_NAME}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_NAME: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    {/* <div className="data-label"> Client Email ID From:</div>
                  <div>
                    <input
                      type="email"
                      value={patent.Client_Email_ID_From}
                      onChange={(e) => setFrom(e.target.value)}
                    />
                  </div> */}
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Email ID To:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={to}
                          onChange={(e) => setTo(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold">
                        {" "}
                        Client Email ID CC{" "}
                      </div>
                      <div className="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={cc}
                          onChange={(e) => setCC(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold">
                        {" "}
                        Client Email ID BCC:
                      </div>
                      <div className="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={bcc}
                          onChange={(e) => setBCC(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold"> Subject:</div>
                      <div className="mt-1">
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={subject}
                          onChange={(e) => setSubject(e.target.value)}
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Text:</div>
                      <div class="mt-1">
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={text}
                          onChange={(e) => setText(e.target.value)}
                        />
                      </div>
                      <br></br>
                      <button
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded"
                        onClick={handleEmailSubmit}
                      >
                        Send Email
                      </button>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Application No</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICATION_NUMBER}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Applicant Name:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.applicants.map(({ APPLICANT_NAME }) => (
                          <div key={APPLICANT_NAME}>{APPLICANT_NAME}</div>
                        ))}
                      </div>
                    </div>

                    {/* <div class="col-span-1 p-4 border border-solid border-gray-400">
                    <div class="text-lg font-semibold">Inventor Name:</div>
                    <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                      {patent.inventors.map(({ INVENTOR_NAME }) => (
                        <div key={INVENTOR_NAME}>{INVENTOR_NAME}</div>
                      ))}
                    </div>
                  </div> */}

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Application Type:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICATION_TYPE}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Applicant Type:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICANT_TYPE}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        Title of Invention
                      </div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.TITLE_OF_INVENTION}
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Group</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.Group}
                          readOnly={!editable}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Group: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Date of Filing:</div>
                      <div class="mt-1 ">
                        <input
                          type="date"
                          readOnly={!editable}
                          value={patent.DATE_OF_FILING}
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Controller Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent?.Controller_Name}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Controller_Name: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Examiner Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent?.EXAMINER}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                EXAMINER: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        FER Issue Date: <span class="text-red-500">*</span>
                      </div>
                      <div class="mt-1">
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FER_Issue_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FER_Issue_Date: value,
                              }))
                            )
                          }
                          required
                        />
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Formal Requirement Intimation Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FORMAL_REQUIREMENT_INTIMATION_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FORMAL_REQUIREMENT_INTIMATION_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Reviewer Mail STC Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.REVIEWER_MAIL_STC_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REVIEWER_MAIL_STC_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        Client instructions for Non -Compliance of Formals:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={
                            patent?.Client_Instruction_for_Non_Compliance_of_Formals
                          }
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Client_Instruction_for_Non_Compliance_of_Formals:
                                  value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        No of Cited Documents{" "}
                        <span class="text-red-500">*</span>
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.NO_OF_CITED_DOCUMENTS}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                NO_OF_CITED_DOCUMENTS: value,
                              }))
                            )
                          }
                          required
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Reviewer Comments:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent?.REVIEWER_COMMENTS}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REVIEWER_COMMENTS: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Due Date for FER Reply:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.DUE_DATE_FOR_FER_Reply_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                DUE_DATE_FOR_FER_Reply_Date: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Inform Client Due Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INFORM_CLIENT_DUE_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INFORM_CLIENT_DUE_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Informed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_INFORMED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_INFORMED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Latest Client Mail Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.LATEST_CLIENT_MAIL_DATE}
                          onChange={handleClientMailDateChange}
                        ></input>
                      </div>
                    </div>

                    {/* <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> NO. OF EMAILS SENT TO CLIENT:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.NO_OF_EMAILS_SENT_TO_CLIENT}
                          readOnly={!editable}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                NO_OF_EMAILS_SENT_TO_CLIENT: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                  </div> */}

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Instruction Received Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_INSTRUCTION_RECIEVED_DATE}
                          onChange={handleAllotment}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Allotment Date:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.ALLOTMENT_DATE}
                          onChange={handleAssociateDeadlineChange}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Associate Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.ASSOCIATE_NAME}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                ASSOCIATE_NAME: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Associate Deadline:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.ASSOCIATE_DEADLINE}
                          onChange={handleFirstDraftSubmissionDateChange}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        First Draft Submission Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FIRST_DRAFT_SUBMISSION_DATE}
                          onChange={handleSTCNotsent}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        First Draft STC for Review Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FIRST_DRAFT_STC_FOR_REVIEW_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FIRST_DRAFT_STC_FOR_REVIEW_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Latest Reminder Sent Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.LATEST_REMINDER_SENT_DATE}
                          onChange={handleClientApproval}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Approval Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={handleClientApprovalDateChange}
                        ></input>
                      </div>
                      {reminderSent && (
                        <div class="col-span-1 p-4 border border-solid border-red-500">
                          <div class="text-lg font-semibold text-red-500">
                            Reminder:
                          </div>
                          <div class="mt-1">Client approval is pending!</div>
                        </div>
                      )}
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Approval Details
                      </div>
                      <div class="mt-1">
                        {" "}
                        <select
                          class="border border-gray-300 rounded-md py-1 px-2"
                          // disabled={!editable}
                          value={patent.CLIENT_APPROVAL_DETAILS}
                          onChange={handleClientApprovalStatusChange}
                        >
                          <option>select</option>
                          <option value="Approval">Approval</option>
                          <option value="Changes Required">
                            Changes Required
                          </option>
                          <option value="DisApproval/Instructions">
                            DisApproval/Instructions{" "}
                          </option>
                          <option value="Application Withdrawn">
                            Withdrawn
                          </option>
                        </select>{" "}
                        <br></br>
                        <br></br>
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 bg-blue-100 text-black"
                          value={patent.CLIENT_APPROVAL_DETAILS}
                          readOnly
                        />
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Standing Instructions by Client:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent?.STANDING_INSTRUCTIONS_BY_CLIENT}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                STANDING_INSTRUCTIONS_BY_CLIENT: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Request for Extension Filed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.REQUEST_FOR_EXTENSION_FILED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REQUEST_FOR_EXTENSION_FILED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Petition under RULE 138 Filed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.PETITION_UNDER_RULE_138_FILED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                PETITION_UNDER_RULE_138_FILED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Application Withdrawn Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.APPLICATION_WITHDRAWN_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                APPLICATION_WITHDRAWN_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> FER Reply Date:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FER_Reply_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FER_Reply_Date: value,
                              }))
                            )
                          }
                          disabled={
                            patent.FER_Issue_Date?.trim() === "" ? true : false
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Invoice Number: </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INVOICE_NUMBER}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INVOICE_NUMBER: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Invoice Generated Date :
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INVOICE_GENERATED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INVOICE_GENERATED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        IPO Application Status:
                      </div>
                      <div class="mt-1">
                        <select
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.IPO_Application_Status}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                IPO_Application_Status: value,
                              }))
                            )
                          }
                        >
                          <option value="">Select </option>
                          <option value="FER Issued, Reply not Filed">
                            FER Issued, Reply not Filed
                          </option>
                          <option value="Fer issued and Client to be informed">
                            FER Issued & Client to be informed
                          </option>
                          <option value="Client informed and Client instruction awaiitng">
                            Client Informed & Client Instruction Awaiitng
                          </option>
                          <option value="Client not responding">
                            Client Not-Responding
                          </option>
                          <option value="Client instruction received">
                            Client Instruction Received
                          </option>
                          <option value="Allotted to Associate">
                            Allotted to Associate
                          </option>
                          <option value="Pending with Associate">
                            Pending with Associate
                          </option>
                          <option value="Pending with Reviewer">
                            Pending with Reviewer
                          </option>
                          <option value="Final draft Received ">
                            Final draft Received
                          </option>
                          <option value="FER reply filed">
                            FER Reply Filed
                          </option>
                          <option value="Reply Filed. Application in amended examination">
                            Reply Filed. Application in amended examination
                          </option>
                        </select>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Controller Reply:
                      </div>
                      <div clclass="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.Controller_Reply}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Controller_Reply: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Client Response</div>
                      <div class="mt-1">
                        {" "}
                        <select
                          value={patent.CLIENT_RESPONSE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_RESPONSE: value,
                              }))
                            )
                          }
                        >
                          <option value="interested" selected>
                            Interested
                          </option>
                          <option value="not interested">Not Interested</option>
                          <option value="not interested">Not Responding</option>
                        </select>{" "}
                      </div>
                    </div>
                  </div>
                  <div class="col-span-1 p-4 border border-solid border-gray-400 mt-5">
                    <div class="text-lg font-semibold">File:</div>
                    <div class="mt-1">
                      <input
                        type="file"
                        accept=".pdf"
                        onChange={handleFileChange}
                        multiple
                        className="border-b-2 border-gray-300 focus:outline-none focus:border-blue-500"
                      />
                      {patent.file?.map(({ name, data }, index) => (
                        <React.Fragment key={index}>
                          {data.startsWith("data:application/pdf;base64,") ? (
                            <a
                              href={data}
                              target="_blank"
                              rel="noopener noreferrer"
                              className="underline text-blue-500"
                            >
                              {index + 1}) {name}
                            </a>
                          ) : (
                            <a
                              href={`data:application/pdf;base64,${data}`}
                              target="_blank"
                              rel="noopener noreferrer"
                              className="underline text-blue-500"
                            >
                              {index + 1}) {name}
                            </a>
                          )}
                          &nbsp; &nbsp; &nbsp;
                        </React.Fragment>
                      ))}
                    </div>
                  </div>

                  <div class="col-span-1 p-4 border border-solid border-gray-400">
                    <div class="text-lg font-semibold">
                      {" "}
                      Special Client or Regular Client
                    </div>
                    <div class="mt-1">
                      {" "}
                      <select
                        value={patent.Special_Client_or_Regular_Client}
                        onChange={(e) =>
                          updateData(e, (value) =>
                            setPatent((prevPatent) => ({
                              ...prevPatent,
                              Special_Client_or_Regular_Client: value,
                            }))
                          )
                        }
                      >
                        <option>select</option>
                        <option value="Yes">YES</option>
                        <option value="No">NO</option>
                      </select>{" "}
                    </div>
                  </div>
                  {/* <div class="col-span-1 p-4 border border-solid border-gray-400"> */}
                  <div className="remark mt-10 ">
                    <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
                      {" "}
                      RKD remark:
                    </div>
                    <div className="rkd-remark-content p-4 border border-solid border-gray-400">
                      <table className="border-collapse border border-gray-300 w-full">
                        <tbody>
                          <tr>
                            <td className="border border-gray-300 px-4 py-2">
                              <input
                                type="text"
                                value={remark.Remark}
                                placeholder="Write Remark..."
                                onChange={(e) =>
                                  updateData(e, (value) =>
                                    setRemark((prevRemark) => ({
                                      ...prevRemark,
                                      Remark: value,
                                    }))
                                  )
                                }
                                className="w-full focus:outline-none py-1 px-2"
                              />
                            </td>
                            <td className="border border-gray-300 px-4 py-2 text-center">
                              <button
                                className={`add-btn py-1 px-2 rounded bg-blue-500 text-white 
                              }`}
                                onClick={addRemark}
                              >
                                Add
                              </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      {patent.ferRemarks?.length > 0 ? (
                        <table className="border-collapse border border-gray-300 mt-4 w-full">
                          <thead>
                            <tr>
                              <th className="border border-gray-300 px-4 py-2">
                                No
                              </th>
                              <th className="border border-gray-300 px-4 py-2">
                                Date
                              </th>
                              <th className="border border-gray-300 px-4 py-2">
                                Remark
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {patent.ferRemarks.map(
                              ({ Date, Remark }, index) => {
                                return (
                                  <tr key={index}>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {index + 1}
                                    </td>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {Date}
                                    </td>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {Remark}
                                    </td>
                                  </tr>
                                );
                              }
                            )}
                          </tbody>
                        </table>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>

                  <div className="mail mt-10">
                    <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
                      Follow up Mail Log
                    </div>

                    <div className="data-date-date overflow-x-auto p-4 border border-solid border-gray-400">
                      <table className="w-full">
                        <tr>
                          <td className="px-4 py-2">
                            <input
                              type="text"
                              value={mailLog.Mail}
                              placeholder="Mail"
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Mail: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="date"
                              value={mailLog.Date_1}
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Date_1: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="text"
                              value={mailLog.response}
                              placeholder="Response"
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    response: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="date"
                              value={mailLog.Date_2}
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Date_2: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <button
                              onClick={addMail}
                              className={`py-1 px-2 rounded bg-blue-500 text-white 
                            }`}
                            >
                              Add
                            </button>
                          </td>
                        </tr>
                      </table>
                      {/* give good css to below table it should be scrollable */}
                      {patent.mailLogs2?.length > 0 ? (
                        <table className="mail-table w-full">
                          <thead>
                            <tr>
                              <th className="border px-4 py-2">Sr. No</th>
                              <th className=" border px-4 py-2">Mail</th>
                              <th className="border px-4 py-2">
                                Mail Sent Date
                              </th>
                              <th className="border px-4 py-2">Response</th>
                              <th className="border px-4 py-2">
                                Response Date
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {patent.mailLogs2.map(
                              ({ Mail, Date_1, response, Date_2 }, index) => (
                                <tr key={index}>
                                  <td className="border px-4 py-2 text-center">
                                    {index + 1}
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="text"
                                      value={Mail}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Mail = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="date"
                                      value={Date_1}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Date_1 = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="text"
                                      value={response}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].response =
                                              value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="date"
                                      value={Date_2}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Date_2 = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                </tr>
                              )
                            )}
                          </tbody>
                        </table>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>

                  <div className="flex justify-center mt-11">
                    <div className="saveButton">
                      <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        onClick={handleSave}
                      >
                        Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>

          {/* I want it in proper tabular format like, this popup will get open when user select stage and hit search button I want that if content is large it should bbe scrollable also content shoulld not get ouside the popup window */}
          <div className="stagePopup">
            {currentPopup === "popup3" && (
              <div className="stagepopup">
                <div className="popup-content bg-white shadow-md rounded-md p-4 font-serif font-times">
                  <div className="popup-header relative mb-4">
                    <div className="pop-header text-3xl font-semibold bg-yellow-300 p-4 rounded-lg text-center">
                      {searchStage || "Applications"}
                    </div>

                    <button
                      className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 close cursor-pointer absolute top-0 right-10 mt-2 "
                      onClick={downloadExcel}
                    >
                      Download
                    </button>
                    <span
                      className="close cursor-pointer absolute top-0 right-0 p-3 text-3xl"
                      onClick={ClosePopup}
                    >
                      &times;
                    </span>
                  </div>

                  <div className="popup-header mt-4">
                    <div className="flex items-center justify-center space-x-4 bg-gray-100">
                      <div className="text-lg font-semibold text-gray-800">
                        Filters:
                      </div>

                      <div className="flex space-x-4">
                        <div className="flex items-center space-x-2">
                          <select
                            className="px-2 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                            value={filterStage}
                            onChange={(e) =>
                              updateData(e, (value) => setFilterStage(value))
                            }
                          >
                            <option value="select">Select</option>
                            <option value="DATE_OF_FILING">
                              DATE OF FILING
                            </option>
                            <option value="DUE_DATE_OF_PUBLICATION">
                              DUE DATE OF PUBLICATION
                            </option>
                            <option value="PUBLICATION_DATE_U_S_11A">
                              PUBLICATION DATE
                            </option>
                            <option value="REQUEST_FOR_EXAMINATION_DATE">
                              REQUEST FOR EXAMINATION DATE
                            </option>
                            <option value="FER_Issue_Date">
                              FER Issue Date
                            </option>
                            <option value="DUE_DATE_FOR_FER_Reply_Date">
                              DUE DATE FOR FER Reply Date
                            </option>
                            <option value="FER_Reply_Date">
                              FER Reply Date
                            </option>
                            <option value="Hearing_Date">Hearing Date</option>
                            <option value="WS_filing_date">
                              WS filing date
                            </option>
                          </select>
                        </div>
                        <div>
                          <div className="text-sm">From:</div>
                          <input
                            type="date"
                            className="px-2 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                            value={filterFrom}
                            onChange={(e) =>
                              updateData(e, (value) => setFilterFrom(value))
                            }
                          />
                        </div>
                        <div>
                          <div className="text-sm">To:</div>
                          <input
                            type="date"
                            className="px-2 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                            value={filterTo}
                            onChange={(e) =>
                              updateData(e, (value) => setFilterTo(value))
                            }
                          />
                        </div>
                      </div>
                      <div className="flex space-x-4">
                        <button
                          className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 flex items-center space-x-2"
                          onClick={filterData}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-4 w-4"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth={2}
                              d="M5 13l4 4L19 7"
                            />
                          </svg>
                          <span>Filter</span>
                        </button>
                        <button
                          className="bg-gray-300 text-gray-800 px-4 py-2 rounded-md hover:bg-gray-400 flex items-center space-x-2"
                          onClick={removeFilter}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            className="h-4 w-4"
                            viewBox="0 0 20 20"
                            fill="currentColor"
                          >
                            <path
                              fillRule="evenodd"
                              d="M10 3a1 1 0 00-1 1v10a1 1 0 102 0V4a1 1 0 00-1-1zM4 9a1 1 0 011-1h10a1 1 0 110 2H5a1 1 0 01-1-1z"
                              clipRule="evenodd"
                            />
                          </svg>
                          <span>Remove Filter</span>
                        </button>
                      </div>
                    </div>
                    <div>
                      <input
                        type="text"
                        onChange={(e) =>
                          updateData(e, (value) => setSearchAll(value))
                        }
                      ></input>
                      <button>Search</button>
                    </div>
                  </div>

                  <div className="mt-5">
                    <table class="table-auto max-w-full divide-y divide-gray-200 text-xxxs border border-collapse border-black">
                      <thead>
                        <tr className="bg-gray-200 text-gray-700">
                          <th className=" border border-black px-3 py-4">
                            Application No.
                          </th>
                          <th className=" border border-black px-3  py-4">
                            Date
                          </th>
                          <th className=" border border-black px-3 py-4">
                            Client Name
                          </th>
                          <th className=" border border-black px-3 py-4">
                            Client Email ID
                          </th>
                          <th className=" border border-black px-3  py-2">
                            Applicant Name
                          </th>
                          <th className=" border border-black px-3 py-4">
                            Applicant Type
                          </th>
                          {/* <th className=" border border-black  px-3  py-2">
                             Inventor Name
                           </th> */}
                          <th className=" border border-black  px-3  py-2">
                            Title of Invention
                          </th>
                          <th className=" border border-black  px-3  py-2">
                            Field
                          </th>
                          <th className=" border border-black  px-3  py-2">
                            Controller Name
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Examiner Name
                          </th>
                          <th className="border border-black  px-3  py-2">
                            FER Issue Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Formal Objection Reasons
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Formal Requirement Intimation Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Reviewer Mail STC Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Instructions for Non-Compliance of Formals
                          </th>
                          <th className="border border-black  px-3  py-2">
                            No. of Cited Documents
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Reviewer Comments
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Due date for FER Reply
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Informed Due Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Informed Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Latest Client Mail Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Instruction Received Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Allotment Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Associate Name
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Associate Deadline
                          </th>
                          <th className="border border-black  px-3  py-2">
                            First Draft Submission Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            First Draft STC for Review Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Latest Reminder Sent Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Approval Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Approval Details
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Standing Instructions by Client
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Request for Extension Filed Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Petition under RULE 138 Filed Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Application Withdrawn Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            FER Reply Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Invoice Number
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Invoice Generated Date
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Special Client or Regular Client
                          </th>
                          <th className="border border-black  px-3  py-2">
                            IPO Application Status
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Status
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Client Response
                          </th>
                          <th className="border border-black  px-3  py-2">
                            Update
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {patents.length > 0 ? (
                          patents.map((patent, index) => (
                            <tr
                              key={index}
                              className="text-gray-800 hover:bg-gray-100 cursor-pointer"
                              onClick={() => showUpdate(index)}
                            >
                              <td
                                className="border border-black  px-3 py-2 overflow-wrap-break-word underline"
                                onDoubleClick={() => {
                                  showUpdate(index);
                                  displayPopup("popup4");
                                }}
                              >
                                {patent.APPLICATION_NUMBER}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.Date}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_NAME}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.Client_Email_ID_To}
                              </td>

                              <td className="border border-black  px-3 py-2">
                                {patent.applicants.map(
                                  ({ APPLICANT_NAME }, i) => (
                                    <span key={i} className="block">
                                      {APPLICANT_NAME}
                                    </span>
                                  )
                                )}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.APPLICANT_TYPE}
                              </td>
                              <td className="border border-black  px-3 py-2 overflow-wrap-break-word">
                                {patent.TITLE_OF_INVENTION}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.Group}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.Controller_Name}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.EXAMINER}
                              </td>
                              <td className="border border-black px-3 py-2">
                                {patent.FER_Issue_Date}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.FORMAL_OBJECTION_REASONS}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.FORMAL_REQUIREMENT_INTIMATION_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.REVIEWER_MAIL_STC_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {
                                  patent.Client_Instruction_for_Non_Compliance_of_Formals
                                }
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.NO_OF_CITED_DOCUMENTS}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.REVIEWER_COMMENTS}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.DUE_DATE_FOR_FER_Reply_Date}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.INFORM_CLIENT_DUE_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_INFORMED_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.LATEST_CLIENT_MAIL_DATE}
                              </td>
                              {/* <td className="border border-black  px-3 py-2">
                                 {patent.NO_OF_EMAILS_SENT_TO_CLIENT}
                               </td> */}
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_INSTRUCTION_RECIEVED_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.ALLOTMENT_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.ASSOCIATE_NAME}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.ASSOCIATE_DEADLINE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.FIRST_DRAFT_SUBMISSION_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.FIRST_DRAFT_STC_FOR_REVIEW_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.LATEST_REMINDER_SENT_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_APPROVAL_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_APPROVAL_DETAILS}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.STANDING_INSTRUCTIONS_BY_CLIENT}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.REQUEST_FOR_EXTENSION_FILED_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.PETITION_UNDER_RULE_138_FILED_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.APPLICATION_WITHDRAWN_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.FER_Reply_Date}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.INVOICE_NUMBER}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.INVOICE_GENERATED_DATE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.Special_Client_or_Regular_Client}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.IPO_Application_Status}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.STATUS}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                {patent.CLIENT_RESPONSE}
                              </td>
                              <td className="border border-black  px-3 py-2">
                                <button
                                  onClick={() => {
                                    showUpdate(index);
                                    displayPopup("popup4");
                                  }}
                                  className="update-button bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                >
                                  Update
                                </button>
                              </td>
                            </tr>
                          ))
                        ) : (
                          <tr>
                            <td colSpan="23" className="px-4 py-2 text-center">
                              <h3 className="font-bold text-3xl text-black">
                                No data available
                              </h3>
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                </div>{" "}
              </div>
            )}
          </div>

          <div className="updatePopup">
            {currentPopup === "popup4" && (
              <div className="popup">
                {/* <button onClick={ClosePopup}>Close</button> */}
                {/* I want this pop-header in goot color,center and when i scroll, it should be fixed */}

                <div className="popup-content bg-white shadow-md rounded-md p-4">
                  <div className="popup-header relative mb-4">
                    <div className="pop-header text-3xl font-semibold bg-yellow-300 p-4 rounded-lg text-center">
                      Application Update{" "}
                    </div>
                    <span
                      className="close cursor-pointer absolute top-0 right-0 p-3 text-3xl"
                      onClick={ClosePopup}
                    >
                      &times;
                    </span>
                  </div>
                  {/* in popup-data there are 21 fields i want first 20 fields side by side means there will be four columns 1.data-label 2.data-data 3.data-label 4.data-data and last field status at bttom with its data big and bold */}
                  <div class="grid grid-cols-4 gap-4">
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        FER Info Enter Date:
                      </div>
                      <div class="mt-1 ">
                        <input
                          type="date"
                          readOnly={!editable}
                          value={patent.Date}
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Client Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_NAME}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_NAME: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    {/* <div className="data-label"> Client Email ID From:</div>
                <div>
                  <input
                    type="email"
                    value={patent.Client_Email_ID_From}
                    onChange={(e) => setFrom(e.target.value)}
                  />
                </div> */}
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Email ID To:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={to}
                          onChange={(e) => setTo(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold">
                        {" "}
                        Client Email ID CC{" "}
                      </div>
                      <div className="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={cc}
                          onChange={(e) => setCC(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold">
                        {" "}
                        Client Email ID BCC:
                      </div>
                      <div className="mt-1">
                        {" "}
                        <input
                          type="email"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={bcc}
                          onChange={(e) => setBCC(e.target.value)}
                          placeholder="Separate multiple emails with commas"
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div className="text-lg font-semibold"> Subject:</div>
                      <div className="mt-1">
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={subject}
                          onChange={(e) => setSubject(e.target.value)}
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Text:</div>
                      <div class="mt-1">
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={text}
                          onChange={(e) => setText(e.target.value)}
                        />
                      </div>
                      <br></br>
                      <button
                        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded"
                        onClick={handleEmailSubmit}
                      >
                        Send Email
                      </button>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Application No</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICATION_NUMBER}
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Status </div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {" "}
                        {patent.STATUS}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Applicant Name:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.applicants.map(({ APPLICANT_NAME }) => (
                          <div key={APPLICANT_NAME}>{APPLICANT_NAME}</div>
                        ))}
                      </div>
                    </div>

                    {/* <div class="col-span-1 p-4 border border-solid border-gray-400">
                  <div class="text-lg font-semibold">Inventor Name:</div>
                  <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                    {patent.inventors.map(({ INVENTOR_NAME }) => (
                      <div key={INVENTOR_NAME}>{INVENTOR_NAME}</div>
                    ))}
                  </div>
                </div> */}

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Application Type:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICATION_TYPE}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Applicant Type:</div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.APPLICANT_TYPE}
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        Title of Invention
                      </div>
                      <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
                        {patent.TITLE_OF_INVENTION}
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Group</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.Group}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Group: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">Date of Filing:</div>
                      <div class="mt-1 ">
                        <input
                          type="date"
                          value={patent.DATE_OF_FILING}
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Controller Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.Controller_Name}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Controller_Name: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>
                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Examiner Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.EXAMINER}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                EXAMINER: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        FER Issue Date: <span class="text-red-500">*</span>{" "}
                      </div>
                      <div class="mt-1">
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FER_Issue_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FER_Issue_Date: value,
                              }))
                            )
                          }
                          disabled={
                            patent.REQUEST_FOR_EXAMINATION_DATE.trim() === ""
                              ? true
                              : false
                          }
                          required
                        />
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Formal Requirement Intimation Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FORMAL_REQUIREMENT_INTIMATION_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FORMAL_REQUIREMENT_INTIMATION_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Reviewer Mail STC Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.REVIEWER_MAIL_STC_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REVIEWER_MAIL_STC_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        Client instructions for Non -Compliance of Formals:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={
                            patent.Client_Instruction_for_Non_Compliance_of_Formals
                          }
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Client_Instruction_for_Non_Compliance_of_Formals:
                                  value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        No of Cited Documents{" "}
                        <span class="text-red-500">*</span>
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.NO_OF_CITED_DOCUMENTS}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                NO_OF_CITED_DOCUMENTS: value,
                              }))
                            )
                          }
                          required
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Reviewer Comments:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.REVIEWER_COMMENTS}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REVIEWER_COMMENTS: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Due Date for FER Reply:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.DUE_DATE_FOR_FER_Reply_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                DUE_DATE_FOR_FER_Reply_Date: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Inform Client Due Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INFORM_CLIENT_DUE_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INFORM_CLIENT_DUE_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Informed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_INFORMED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_INFORMED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Latest Client Mail Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.LATEST_CLIENT_MAIL_DATE}
                          onChange={handleClientMailDateChange}
                        ></input>
                      </div>
                    </div>

                    {/* <div class="col-span-1 p-4 border border-solid border-gray-400">
                    <div class="text-lg font-semibold"> NO. OF EMAILS SENT TO CLIENT:</div>
                    <div class="mt-1">
                      {" "}
                      <input
                        type="text"
                        class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                        value={patent.NO_OF_EMAILS_SENT_TO_CLIENT}
                        onChange={(e) =>
                          updateData(e, (value) =>
                            setPatent((prevPatent) => ({
                              ...prevPatent,
                              NO_OF_EMAILS_SENT_TO_CLIENT: value,
                            }))
                          )
                        }
                      ></input>
                    </div>
                </div> */}

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Instruction Received Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_INSTRUCTION_RECIEVED_DATE}
                          onChange={handleAllotment}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Allotment Date:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={handleAssociateDeadlineChange}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Associate Name:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.ASSOCIATE_NAME}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                ASSOCIATE_NAME: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Associate Deadline:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.ASSOCIATE_DEADLINE}
                          onChange={handleFirstDraftSubmissionDateChange}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        First Draft Submission Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FIRST_DRAFT_SUBMISSION_DATE}
                          onChange={handleSTCNotsent}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        First Draft STC for Review Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FIRST_DRAFT_STC_FOR_REVIEW_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FIRST_DRAFT_STC_FOR_REVIEW_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Latest Reminder Sent Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.LATEST_REMINDER_SENT_DATE}
                          onChange={handleClientApproval}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Approval Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.CLIENT_APPROVAL_DATE}
                          onChange={handleClientApprovalDateChange}
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Client Approval Details
                      </div>
                      <div class="mt-1">
                        {" "}
                        <select
                          class="border border-gray-300 rounded-md py-1 px-2"
                          value={patent.CLIENT_APPROVAL_DETAILS}
                          onChange={handleClientApprovalStatusChange}
                        >
                          <option>select</option>
                          <option value="Approval">Approval</option>
                          <option value="Changes Required">
                            Changes Required
                          </option>
                          <option value="DisApproval">
                            DisApproval/Instructions{" "}
                          </option>
                          <option value="Withdrawn">
                            Application Withdrawn
                          </option>
                        </select>{" "}
                        <br></br>
                        <br></br>
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500 bg-blue-100 text-black"
                          value={patent.CLIENT_APPROVAL_DETAILS}
                          readOnly
                        />
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Standing Instructions by Client:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.STANDING_INSTRUCTIONS_BY_CLIENT}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                STANDING_INSTRUCTIONS_BY_CLIENT: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Request for Extension Filed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.REQUEST_FOR_EXTENSION_FILED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                REQUEST_FOR_EXTENSION_FILED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Petition under RULE 138 Filed Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.PETITION_UNDER_RULE_138_FILED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                PETITION_UNDER_RULE_138_FILED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Application Withdrawn Date:
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.APPLICATION_WITHDRAWN_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                APPLICATION_WITHDRAWN_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> FER Reply Date:</div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.FER_Reply_Date}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                FER_Reply_Date: value,
                              }))
                            )
                          }
                          disabled={
                            patent.FER_Issue_Date.trim() === "" ? true : false
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Invoice Number: </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INVOICE_NUMBER}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INVOICE_NUMBER: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Invoice Generated Date :
                      </div>
                      <div class="mt-1">
                        {" "}
                        <input
                          type="date"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.INVOICE_GENERATED_DATE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                INVOICE_GENERATED_DATE: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        IPO Application Status:
                      </div>
                      <div class="mt-1">
                        <select
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.IPO_Application_Status}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                IPO_Application_Status: value,
                              }))
                            )
                          }
                        >
                          <option value="">Select </option>
                          <option value="FER Issued, Reply not Filed">
                            FER Issued, Reply not Filed
                          </option>
                          <option value="Fer issued and Client to be informed">
                            FER Issued & Client to be informed
                          </option>
                          <option value="Client informed and Client instruction awaiitng">
                            Client Informed & Client Instruction Awaiitng
                          </option>
                          <option value="Client not responding">
                            Client Not-Responding
                          </option>
                          <option value="Client instruction received">
                            Client Instruction Received
                          </option>
                          <option value="Allotted to Associate">
                            Allotted to Associate
                          </option>
                          <option value="Pending with Associate">
                            Pending with Associate
                          </option>
                          <option value="Pending with Reviewer">
                            Pending with Reviewer
                          </option>
                          <option value="Final draft Received ">
                            Final draft Received
                          </option>
                          <option value="FER reply filed">
                            FER Reply Filed
                          </option>
                          <option value="Reply Filed. Application in amended examination">
                            Reply Filed. Application in amended examination
                          </option>
                        </select>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold">
                        {" "}
                        Controller Reply:
                      </div>
                      <div clclass="mt-1">
                        {" "}
                        <input
                          type="text"
                          class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                          value={patent.Controller_Reply}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                Controller_Reply: value,
                              }))
                            )
                          }
                        ></input>
                      </div>
                    </div>

                    <div class="col-span-1 p-4 border border-solid border-gray-400">
                      <div class="text-lg font-semibold"> Client Response</div>
                      <div class="mt-1">
                        {" "}
                        <select
                          value={patent.CLIENT_RESPONSE}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setPatent((prevPatent) => ({
                                ...prevPatent,
                                CLIENT_RESPONSE: value,
                              }))
                            )
                          }
                        >
                          <option value="interested" selected>
                            Interested
                          </option>
                          <option value="not interested">Not Interested</option>
                          <option value="not interested">Not Responding</option>
                        </select>{" "}
                      </div>
                    </div>
                  </div>
                  <div class="col-span-1 p-4 border border-solid border-gray-400 mt-5">
                    <div class="text-lg font-semibold">File:</div>
                    <div class="mt-1">
                      <input
                        type="file"
                        accept=".pdf"
                        onChange={handleFileChange}
                        multiple
                        className="border-b-2 border-gray-300 focus:outline-none focus:border-blue-500"
                      />
                      {patent.file?.map(({ name, data }, index) => (
                        <React.Fragment key={index}>
                          {data.startsWith("data:application/pdf;base64,") ? (
                            <a
                              href={data}
                              target="_blank"
                              rel="noopener noreferrer"
                              className="underline text-blue-500"
                            >
                              {index + 1}) {name}
                            </a>
                          ) : (
                            <a
                              href={`data:application/pdf;base64,${data}`}
                              target="_blank"
                              rel="noopener noreferrer"
                              className="underline text-blue-500"
                            >
                              {index + 1}) {name}
                            </a>
                          )}
                          &nbsp; &nbsp; &nbsp;
                        </React.Fragment>
                      ))}
                    </div>
                  </div>

                  <div class="col-span-1 p-4 border border-solid border-gray-400">
                    <div class="text-lg font-semibold">
                      {" "}
                      Special Client or Regular Client
                    </div>
                    <div class="mt-1">
                      {" "}
                      <select
                        value={patent.Special_Client_or_Regular_Client}
                        onChange={(e) =>
                          updateData(e, (value) =>
                            setPatent((prevPatent) => ({
                              ...prevPatent,
                              Special_Client_or_Regular_Client: value,
                            }))
                          )
                        }
                      >
                        <option>select</option>
                        <option value="YES">YES</option>
                        <option value="NO">NO</option>
                      </select>{" "}
                    </div>
                  </div>
                  {/* <div class="col-span-1 p-4 border border-solid border-gray-400"> */}
                  <div className="remark mt-10 ">
                    <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
                      {" "}
                      RKD remark:
                    </div>
                    <div className="rkd-remark-content p-4 border border-solid border-gray-400">
                      <table className="border-collapse border border-gray-300 w-full">
                        <tbody>
                          <tr>
                            <td className="border border-gray-300 px-4 py-2">
                              <input
                                type="text"
                                value={remark.Remark}
                                placeholder="Write Remark..."
                                onChange={(e) =>
                                  updateData(e, (value) =>
                                    setRemark((prevRemark) => ({
                                      ...prevRemark,
                                      Remark: value,
                                    }))
                                  )
                                }
                                className="w-full focus:outline-none py-1 px-2"
                              />
                            </td>
                            <td className="border border-gray-300 px-4 py-2 text-center">
                              <button
                                className={`add-btn py-1 px-2 rounded bg-blue-500 text-white 
                            }`}
                                onClick={addRemark}
                              >
                                Add
                              </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      {patent.ferRemarks?.length > 0 ? (
                        <table className="border-collapse border border-gray-300 mt-4 w-full">
                          <thead>
                            <tr>
                              <th className="border border-gray-300 px-4 py-2">
                                No
                              </th>
                              <th className="border border-gray-300 px-4 py-2">
                                Date
                              </th>
                              <th className="border border-gray-300 px-4 py-2">
                                Remark
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {patent.ferRemarks.map(
                              ({ Date, Remark }, index) => {
                                return (
                                  <tr key={index}>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {index + 1}
                                    </td>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {Date}
                                    </td>
                                    <td className="border border-gray-300 px-4 py-2 text-center">
                                      {Remark}
                                    </td>
                                  </tr>
                                );
                              }
                            )}
                          </tbody>
                        </table>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>

                  <div className="mail mt-10">
                    <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
                      Follow up Mail Log
                    </div>

                    <div className="data-date-date overflow-x-auto p-4 border border-solid border-gray-400">
                      <table className="w-full">
                        <tr>
                          <td className="px-4 py-2">
                            <input
                              type="text"
                              value={mailLog.Mail}
                              placeholder="Mail"
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Mail: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="date"
                              value={mailLog.Date_1}
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Date_1: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="text"
                              value={mailLog.response}
                              placeholder="Response"
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    response: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <input
                              type="date"
                              value={mailLog.Date_2}
                              onChange={(e) =>
                                updateData(e, (value) =>
                                  setmailLog((prev) => ({
                                    ...prev,
                                    Date_2: value,
                                  }))
                                )
                              }
                              className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                            />
                          </td>
                          <td className="px-4 py-2">
                            <button
                              onClick={addMail}
                              className={`py-1 px-2 rounded bg-blue-500 text-white 
                          }`}
                            >
                              Add
                            </button>
                          </td>
                        </tr>
                      </table>
                      {/* give good css to below table it should be scrollable */}
                      {patent.mailLogs2?.length > 0 ? (
                        <table className="mail-table w-full">
                          <thead>
                            <tr>
                              <th className="border px-4 py-2">Sr. No</th>
                              <th className=" border px-4 py-2">Mail</th>
                              <th className="border px-4 py-2">
                                Mail Sent Date
                              </th>
                              <th className="border px-4 py-2">Response</th>
                              <th className="border px-4 py-2">
                                Response Date
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {patent.mailLogs2.map(
                              ({ Mail, Date_1, response, Date_2 }, index) => (
                                <tr key={index}>
                                  <td className="border px-4 py-2 text-center">
                                    {index + 1}
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="text"
                                      value={Mail}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Mail = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="date"
                                      value={Date_1}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Date_1 = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="text"
                                      value={response}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].response =
                                              value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                  <td className="border px-4 py-2 text-center">
                                    <input
                                      type="date"
                                      value={Date_2}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setPatent((prev) => {
                                            const updatedMails = [
                                              ...prev.mailLogs2,
                                            ];
                                            updatedMails[index].Date_2 = value;
                                            return {
                                              ...prev,
                                              mailLogs2: updatedMails,
                                            };
                                          })
                                        )
                                      }
                                      className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                                    />
                                  </td>
                                </tr>
                              )
                            )}
                          </tbody>
                        </table>
                      ) : (
                        <></>
                      )}
                    </div>
                  </div>

                  <div className="flex justify-center mt-11">
                    <div className="saveButton">
                      <button
                        className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                        onClick={updateApplication}
                      >
                        Update
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        {/* static content 1-Height 10% all the divs should be in one line and at the center and divs should have some gap between them and label should have buttons like design becuase they are clickable*/}
      </div>
    </>
  );
}

export default FER;
