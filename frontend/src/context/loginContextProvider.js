import { useState } from "react";
import LoginContext from "./loginContext";
function LoginContextProvider({ children }) {
  const [isAuthenticated, setIsAuthenticated] = useState();
  return (
    <LoginContext.Provider value={{ isAuthenticated, setIsAuthenticated }}>
      {children}
    </LoginContext.Provider>
  );
}
export default LoginContextProvider;
