import { useCallback, useState, useEffect } from "react";
import PopupContext from "./popupContext";

function PopupContextProvider({ children }) {
  const [company, setCompany] = useState({
    companyNumber: "",
    nameOfCompany: "",
    addresses: [],
    companyWebsite: "",
    linkedInProfileOfCompany: "",
    category: "",
  });
  const setCompanyEmpty = () => {
    setCompany({
      companyNumber: "",
      nameOfCompany: "",
      addresses: [],
      companyWebsite: "",
      linkedInProfileOfCompany: "",
      category: "",
    });
  };

  const [contactPersonDetail, setContactPersonDetails] = useState({
    salutation: "",
    contactPersonName: "",
    designation: "",
    department: "",
    email: "",
    officialMobile: "",
    personalMobile: "",
    linkdinProfile: "",
    typeOfBusiness: "",
    remarks : "",
    preferredModeOfCommunication: [],
  });
  const [mode, setMode] = useState("");
  const [addressDetails, setAddressDetails] = useState({
    companyAddress: "",
    city: "",
    state: "",
    country: "",
    pincode: "",
    officeTelephone: "",
    fieldOfActivity: "",
    contacts: [],
  });
  useEffect(() => console.log("currentPage", currentPage), []);
  const [currentAddressIndex, setCurrentAddressIndex] = useState(null);
  const [showAddressPopup, setShowAddressPopup] = useState(false);
  //This are states from
  const [searchName, setSearchName] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const [companies, setCompanies] = useState([]);
  const [showUpdate, setShowUpdate] = useState(false);
  // useEffect(() => {
  //   fetch("http://192.168.1.191:8000/api/getallforms")
  //     .then((response) => {
  //       if (response.ok) response.json();
  //     })
  //     .then((data) => {
  //       setCompanies(data);
  //       setShowStage(true);
  //     })
  //     .catch((error) => console.log(error));
  // });

  //popup states
  //stage popup state
  const [showStage, setShowStage] = useState(false);
  //search popup state
  const [showSearch, setShowSearch] = useState(false);
  //chart popup
  const [showChart, setShowChart] = useState(false);
  //add popup state
  const [addPopup, setAddPopup] = useState(false);

  //update popup state
  const [updatePopup, setUpdatePopup] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState();
  // const handleTotalSearch = () => {
  //   fetch(`http://192.168.1.191:5000/api/totalpatents?page=${1}&limit=100`)
  //     .then((response) => {
  //       if (!response.ok) {
  //         throw new Error("Network Error");
  //       }

  //       return response.json();
  //     })
  //     .then((data) => {
  //       console.log(data);
  //       console.log(data.count);

  //       setPatents(data.data);
  //       setCurrentPage(data.currentPage);
  //       setTotalPage(data.totalPages);
  //       console.log("data.currentPage", data.currentPage);
  //       console.log("data.totalPages", data.totalPages);
  //       setShowStage(true);
  //       setAddPopup(false);
  //       setUpdatePopup(false);
  //       setShowSearch(false);
  //       setShowChart(false);
  //     })
  //     .catch((error) => {
  //       console.error("Fetch error:", error);
  //     });
  // };

  //This function is use to deal with filters
  // const handleTotalSearchCopy = () => {
  //   console.log(searchStage);
  //   setSearchStage("");
  //   console.log("in search function");
  //   fetch(`http://192.168.1.191:5000/api/totalpatents&page=${1}&limit=100`)
  //     .then((response) => {
  //       if (!response.ok) {
  //         throw new Error("Network Error");
  //       }
  //       console.log(response);
  //       return response.json();
  //     })
  //     .then((data) => {
  //       console.log("hiiiii");
  //       setPatents(data.data);
  //       setCurrentPage(data.currentPage);
  //       setTotalPage(data.totalPages);
  //       console.log("data.currentPage", data.currentPage);
  //       console.log("data.totalPages", data.totalPages);
  //     })
  //     .catch((error) => {
  //       console.error("Fetch error:", error);
  //     });
  // };

  //This function is used to fetch applications using status like hearing scheduled

  //This function used to fetch applications from applicant name
  const handleSearchApplicantName = (name) => {
    console.log(name);

    if (name) {
      fetch(
        `http://192.168.1.191:5000/api/applicantName?APPLICANT_NAME=${encodeURIComponent(
          name
        )}`
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network Error");
          }
          console.log(response);
          return response.json();
        })
        .then((data) => {
          console.log("data.currentrPage", data.currentPage);
          console.log("data.totalPages", data.totalPages);
          setSearchName(name);
          setShowStage(true);
          setAddPopup(false);
          setUpdatePopup(false);
          setShowChart(false);
        })
        .catch((error) => {
          setShowStage(true);
          console.error("Fetch error:", error);
        }); //change riddhi
    }
  };

  const showAdd = () => {
    setCompanyEmpty();
    setAddPopup(true);
    setShowStage(false);
    setUpdatePopup(false);
    setShowSearch(false);
    setShowChart(false);
  };

  const handleShowChart = () => {
    setCompanyEmpty();
    setShowChart(true);
    setShowSearch(false);
    setUpdatePopup(false);
    setShowStage(false);
    setAddPopup(false);
  };
  //This function is used to open search popup
  const handleSearchButton = () => {
    setCompanyEmpty();
    setShowSearch(true);
    setUpdatePopup(false);
    setShowStage(false);
    setAddPopup(false);
    setShowChart(false);
  };

  const updateData = (e, setter) => {
    const value = e.target.value;
    console.log(e.target.type);
    console.log(value);

    setter(value);
  };

  const [companyBackup, setCompanyBackup] = useState([]);

  const handleSearchChange = (e) => {
    setSearchName(e.target.value);
  };

  const handleTotalSearchForPage = () => {
    fetch(`http://192.168.1.191:8000/api/getallforms`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }

        return response.json();
      })
      .then((data) => {
        console.log(data);
        console.log(data.count);

        // setCurrentPage(data.currentPage);
        // setTotalPage(data.totalPages);
        // console.log("data.currentPage", data.currentPage);
        // console.log("data.totalPages", data.totalPages);
        setCompanies(data.data);
        setShowStage(true);
        setAddPopup(false);
        // setUpdatePopup(false);
        // setShowSearch(false);
        // setShowChart(false);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  };

  return (
    <PopupContext.Provider
      value={{
        showChart,
        setShowChart,
        handleShowChart,
        company,
        setCompany,
        searchName,
        setSearchName,
        isOpen,
        setIsOpen,

        showStage,
        setShowStage,
        showSearch,
        setShowSearch,
        addPopup,
        setAddPopup,
        updatePopup,
        setUpdatePopup,

        handleSearchApplicantName,
        showAdd,
        handleSearchButton,
        updateData,
        companyBackup,
        setCompanyBackup,

        handleSearchChange,
        currentPage,
        setCurrentPage,
        totalPage,
        setTotalPage,
        setCurrentPage,
        setTotalPage,

        contactPersonDetail,
        setContactPersonDetails,

        setCompanyEmpty,

        addressDetails,
        setAddressDetails,

        mode,
        setMode,

        showStage,
        setShowStage,
        handleTotalSearchForPage,
        companies,
        setCompanies,
        showUpdate,
        setShowUpdate,
        currentAddressIndex,
        setCurrentAddressIndex,
        showAddressPopup,
        setShowAddressPopup,
      }}
    >
      {children}
    </PopupContext.Provider>
  );
}

export default PopupContextProvider;
