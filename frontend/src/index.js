import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom/cjs/react-router-dom.min";
import "babel-polyfill";
import LoginContextProvider from "./context/loginContextProvider";
import Login from "./login";
import Home from "./home";
import UpdatePopup from "./popup/updatePopup";

import PopupContextProvider from "./context/popContextProvider";
import updatePopup from "./popup/updatePopup";
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <LoginContextProvider>
    <Router>
      <PopupContextProvider>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/update" exact component={UpdatePopup} />
          <Route path="/login" component={Login}></Route>
        </Switch>
      </PopupContextProvider>
    </Router>
  </LoginContextProvider>
);
