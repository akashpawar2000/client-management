import "./App.css";
import React, { useEffect, useContext, useState } from "react";
import SearchPopup from "./popup/searchpopup.js";
import StagePopup from "./popup/stagepopup.js";
import AddPopup from "./popup/addpopup.js";
import UpdatePopup from "./popup/updatePopup.js";
import ChartPopup from "./popup/chartPopup.js";
import StaticContent1 from "./staticcontent/staticcontent1.js";
import App from "./App.js";

import PopupContext from "./context/popupContext.js";

function Home() {
  const {
    patent,
    isOpen,
    setIsOpen,
    showStage,
    setShowStage,
    addPopup,
    showAdd,
    handleTotalSearchForPage,
    companies,
    company,
    setCompany,
    showUpdate,
    setShowUpdate,
  } = useContext(PopupContext);
  const [showAddressPopup, setShowAddressPopup] = useState(false);
  const [address, setAddress] = useState([]);
  const [selectedCompany, setSelectedCompany] = useState();
  const updateApplication = async () => {
    console.log("patent", patent);
    try {
      const response = await fetch(
        "http://192.168.1.191:5000/api/updatepatent",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(patent),
        }
      );

      const responseData = await response.json();

      if (response.status === 200) {
        // Data inserted successfully
        alert("Data updated Successfully");
      }
    } catch (error) {
      // Handle errors
      console.error("Error submitting patent data:", error.message);
    }
  };

  //This function is use to fetch total applicants (static content 2)

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  const serachCompany = (selectedCompany) => {
    console.log("selectedCompany", selectedCompany);
    const filteredCompany = companies.find(
      (company) => company.nameOfCompany == selectedCompany
    );
    setCompany(filteredCompany);
    setShowStage(!showStage);
  };
  useEffect(() => {
    handleTotalSearchForPage();
  }, []);

  return (
    <div className="webpage">
      <App></App>

      <div className="staticContent2 flex flex-wrap p-4 bg-gray-100 justify-between">
        <div className="section flex items-center mr-4 mb-4">
          <button
            className="button bg-green-500 text-white px-4 py-2 rounded-md"
            onClick={showAdd}
          >
            Click here to add new client
          </button>
        </div>
        <div className="section flex items-center mr-4 mb-4">
          {/* <datalist id="allCompanies">
            {companies?.map((company) => (
              <option value={company.nameOfCompany}></option>
            ))}
          </datalist> */}
          <button
            className="button bg-green-500 text-white px-4 py-2 rounded-md"
            onClick={() => setShowStage(!showStage)}
          >
            search Existing contacts
          </button>
        </div>
      </div>

      {/* //This should open below static content 2 and take whole space but requirement is how big data is it should get out of window means no horizontal scrolling */}
      <div className="dynamicContent overflow-hidden">
        {/* <div className="searchPopup">
          {showSearch && <SearchPopup></SearchPopup>}
        </div> */}
        {/* <div className="chartPopup">
          {showChart && <ChartPopup></ChartPopup>}
        </div> */}
        {/* I want it in proper tabular format like, this popup will get open when user select stage and hit search button I want that if content is large it should bbe scrollable also content shoulld not get ouside the popup window */}
        <div className="stagePopup">
          {showStage && <StagePopup></StagePopup>}
        </div>
        <div className="addPopup"> {addPopup && <AddPopup></AddPopup>}</div>
        {showUpdate && <UpdatePopup></UpdatePopup>}
      </div>
    </div>
  );
}

export default React.memo(Home);
