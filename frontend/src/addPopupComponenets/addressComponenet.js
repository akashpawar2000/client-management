import React, { useContext, useEffect, useState } from "react";
import PopupContext from "../context/popupContext.js";
import ContactPopup from "./contactPopup.js";
import ContactContext from "../context/contactContext.js";

function AddressComponent() {
  const {
    company,
    setCompany,
    updateData,
    contactPersonDetail,
    setContactPersonDetails,
    setCompanyEmpty,
    addressDetails,
    setAddressDetails,
    mode,
    setMode,
    companies,
    setCompanies,
  } = useContext(PopupContext);

  const showAddressPopup = () => {
    setAddressPopup(!addressPopup);
  };
  const [activeIndex, setActiveIndex] = useState(null);
  const [indexToUpdate, setIndexToUpdate] = useState(null);
  const [indexToUpdateContact, setIndexToUpdateContact] = useState(null);
  const [contactPopup, setContactPopup] = useState(false);
  const [addressPopup, setAddressPopup] = useState(false);
  const [showAddressesArray, setShowAddressesArray] = useState(false);
  const [isContactModalOpen, setIsContactModalOpen] = useState(false);
  const [selectedContact, setSelectedContact] = useState(null);
  const [contactIndex, setContactIndex] = useState(null);
  const [currentContactIndex, setCurrentContactIndex] = useState(null);
  const [editContact, setEditContact] = useState(false);
  const [currentAddressIndex, setCurrentAddressIndex] = useState(null);
  const handleShowContact = (index, contactIndex) => {
    console.log("index", index);
    console.log("contact index", contactIndex);
    setCurrentAddressIndex(index);
    setCurrentContactIndex(contactIndex);
    setContactPersonDetails(company.addresses[index].contacts[contactIndex]);
    setIsContactModalOpen(true);
  };

  const handleCloseContact = () => {
    setIsContactModalOpen(false);
    setSelectedContact(null);
  };
  const showContactPopup = (index) => {
    setContactPersonDetails({
      salutation: "",
      contactPersonName: "",
      designation: "",
      department: "",
      email: "",
      officialMobile: "",
      personalMobile: "",
      linkdinProfile: "",
      typeOfBusiness: "",
      remarks: "",
      preferredModeOfCommunication: [],
    });
    setContactPopup(!contactPopup);
    setCurrentAddressIndex(index);
  };

  const addPersonDetails = (index) => {
    // console.log("addressindex", index);
    // console.log("address", company.addresses[index]);
    const missingFields = [];
    if (
      !contactPersonDetail.salutation &&
      !contactPersonDetail.contactPersonName
    )
      missingFields.push("Contact Person Name");
    if (!contactPersonDetail.designation) missingFields.push("Designation");
    if (!contactPersonDetail.department) missingFields.push("Department");
    if (!contactPersonDetail.email) missingFields.push("Email");
    if (!contactPersonDetail.officialMobile)
      missingFields.push("Official Mobile Number");
    if (!contactPersonDetail.typeOfBusiness)
      missingFields.push("Type of Business");
    if (missingFields.length > 0) {
      alert(missingFields.join(",") + " " + "can not be empty");
    } else {
      company.addresses[index].contacts.push(contactPersonDetail);
      setCompany((prev) => ({
        ...prev,
        addresses: [
          ...company.addresses.slice(0, index),
          company.addresses[index],
          ...company.addresses.slice(index + 1),
        ],
      }));
      setContactPersonDetails({
        contactPersonName: "",
        designation: "",
        department: "",
        email: "",
        officialMobile: "",
        personalMobile: "",
        linkdinProfile: "",
        typeOfBusiness: "",
      });
      setActiveIndex(null);
      setContactPopup(!contactPopup);
    }
  };

  const addAddress = () => {
    const missingFields = [];
    if (!addressDetails.companyAddress) missingFields.push("Company Address");
    if (!addressDetails.officeTelephone) missingFields.push("Office Telephone");
    if (missingFields.length > 0) {
      alert(missingFields.join(",") + " " + "can not be empty");
    } else {
      setCompany((prevCompany) => ({
        ...prevCompany,
        addresses: [...prevCompany.addresses, addressDetails],
      }));
      setAddressDetails({
        companyAddress: "",
        city: "",
        state: "",
        country: "",
        pincode: "",
        officeTelephone: "",
        fieldOfActivity: "",
        contacts: [],
      });
      setShowAddressesArray(true);
    }
  };

  const editAddress = (index) => {
    if (index === indexToUpdate) setIndexToUpdate(null);
    else {
      setIndexToUpdate(index);
      setAddressDetails(company.addresses[index]);
    }
  };

  const updateAddress = (index) => {
    let missingFields = [];

    if (!addressDetails.companyAddress) {
      missingFields.push("Company Address");
    }
    if (!addressDetails.officeTelephone) {
      missingFields.push("Office Telephone");
    }

    if (missingFields.length > 0) {
      alert(missingFields.join(", ") + " cannot be empty");
    } else {
      alert("Are you sure you want to update Address?");
      setCompany((prev) => ({
        ...prev,
        addresses: [
          ...prev.addresses.slice(0, index),
          addressDetails,
          ...prev.addresses.slice(index + 1),
        ],
      }));
      setIndexToUpdate(null);
    }
  };

  const editAddressContact = (index, contactIndex) => {
    setEditContact(!editContact);
  };

  const updateAddressContact = (index, contactIndex) => {
    const missingFields = [];
    if (
      !contactPersonDetail.salutation &&
      !contactPersonDetail.contactPersonName
    )
      missingFields.push("Contact Person Name");
    if (!contactPersonDetail.designation) missingFields.push("Designation");
    if (!contactPersonDetail.department) missingFields.push("Department");
    if (!contactPersonDetail.email) missingFields.push("Email");
    if (!contactPersonDetail.officialMobile)
      missingFields.push("Official Mobile Number");
    if (!contactPersonDetail.typeOfBusiness)
      missingFields.push("Type of Business");
    if (missingFields.length > 0) {
      alert(missingFields.join(",") + " " + "can not be empty");
    } else {
      alert("Are you sure you want to update");
      const addressToBeUpdated = company.addresses[currentAddressIndex];
      console.log("addressToBeUpdated", addressToBeUpdated);
      addressToBeUpdated.contacts = [
        ...addressToBeUpdated.contacts.slice(0, currentContactIndex),
        contactPersonDetail,
        ...addressToBeUpdated.contacts.slice(currentContactIndex + 1),
      ];
      setCompany((prev) => ({
        ...prev,
        addresses: [
          ...prev.addresses.slice(0, currentAddressIndex),
          addressToBeUpdated,
          ...prev.addresses.slice(currentAddressIndex + 1),
        ],
      }));

      // setContactPersonDetails({
      //   contactPersonName: "",
      //   designation: "",
      //   department: "",
      //   email: "",
      //   officialMobile: "",
      //   personalMobile: "",
      //   linkdinProfile: "",
      //   typeOfBusiness: "",
      // });
    }
  };

  const deleteAddress = (index) => {
    alert("Are you sure you want to delete?");
    setCompany((prev) => ({
      ...prev,
      addresses: [
        ...prev.addresses.slice(0, index),
        ...prev.addresses.slice(index + 1),
      ],
    }));
  };

  const deleteAddressContact = (index, contactIndex) => {
    alert("Are you sure you want to delete?");
    console.log("currentAddressIndex", currentAddressIndex);
    console.log("currentContactIndex", currentContactIndex);
    const addressToBeUpdated = company.addresses[currentAddressIndex];
    addressToBeUpdated.contacts = [
      ...addressToBeUpdated.contacts.slice(0, currentContactIndex),
      ...addressToBeUpdated.contacts.slice(currentContactIndex + 1),
    ];
    setCompany((prev) => ({
      ...prev,
      addresses: [
        ...prev.addresses.slice(0, currentAddressIndex),
        addressToBeUpdated,
        ...prev.addresses.slice(currentAddressIndex + 1),
      ],
    }));
    setIndexToUpdateContact(false);
    setIsContactModalOpen(false);
  };

  const addCompany = async () => {
    try {
      const response = await fetch("http://192.168.1.191:8000/api/addform", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(company),
      });

      const responseData = await response.json();

      if (response.status === 200) {
        alert("Data Saved Successfully");
        setCompanyEmpty();
      } else if (response.status === 500) {
        alert("Company already existed");
      } else {
        throw new Error("An error occurred while submitting company data");
      }
    } catch (error) {
      console.error("Error submitting company data:", error.message);
    }
  };

  const handleCheckboxToggle = (index) => {
    setCompany((prev) => ({
      ...prev,
      businesstypes: prev.businesstypes.map((item, i) =>
        i === index ? { ...item, types: !item.types } : item
      ),
    }));
  };

  const handleAddMode = (index, contactIndex) => {
    if (mode !== "") {
      if (
        company.addresses[index].contacts[
          contactIndex
        ].preferredModeOfCommunication.includes(mode)
      ) {
        alert("This mode of communication is already selected");
        setMode("");
      } else {
        setCompany((prev) => ({
          ...prev,
          addresses: [
            ...prev.addresses.slice(0, index),
            {
              ...prev.addresses[index],
              contacts: [
                ...prev.addresses[index].contacts.slice(0, contactIndex),
                {
                  ...prev.addresses[index].contacts[contactIndex],
                  preferredModeOfCommunication: [
                    ...prev.addresses[index].contacts[contactIndex]
                      .preferredModeOfCommunication,
                    mode,
                  ],
                },
                ...prev.addresses[index].contacts.slice(contactIndex + 1),
              ],
            },
            ...prev.addresses.slice(index + 1),
          ],
        }));
      }
    }
  };

  return (
    <>
      <div className="add-address-header mt-8 mb-4 p-4 bg-gray-300 rounded-lg text-center font-bold text-xl">
        Add Address
      </div>

      <div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-8  rounded-lg  overflow-hidden m-2 p-2">
        <div className="sm:col-span-8">
          <div className="flex items-center">
            <input
              type="text"
              value={addressDetails.companyAddress}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    companyAddress: value,
                  }))
                )
              }
              required
              placeholder="Company Address"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            />
            <span className="text-red-500 ml-1">*</span>
          </div>
        </div>

        <div className="sm:col-span-2">
          <div>
            <input
              type="text"
              value={addressDetails.city}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    city: value,
                  }))
                )
              }
              required
              placeholder="City"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            ></input>
          </div>
        </div>

        <div className="sm:col-span-2">
          <div>
            <input
              type="text"
              value={addressDetails.state}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    state: value,
                  }))
                )
              }
              required
              placeholder="State"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            ></input>
          </div>
        </div>

        <div className="sm:col-span-2">
          <div>
            <input
              type="text"
              value={addressDetails.country}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    country: value,
                  }))
                )
              }
              required
              placeholder="Country"
              list="countries"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            ></input>
          </div>
          <datalist id="countries">
            <option value="Afghanistan" />
            <option value="Albania" />
            <option value="Algeria" />
            <option value="Andorra" />
            <option value="Angola" />
            <option value="Antigua and Barbuda" />
            <option value="Argentina" />
            <option value="Armenia" />
            <option value="Australia" />
            <option value="Austria" />
            <option value="Azerbaijan" />
            <option value="Bahamas" />
            <option value="Bahrain" />
            <option value="Bangladesh" />
            <option value="Barbados" />
            <option value="Belarus" />
            <option value="Belgium" />
            <option value="Belize" />
            <option value="Benin" />
            <option value="Bhutan" />
            <option value="Bolivia" />
            <option value="Bosnia and Herzegovina" />
            <option value="Botswana" />
            <option value="Brazil" />
            <option value="Brunei" />
            <option value="Bulgaria" />
            <option value="Burkina Faso" />
            <option value="Burundi" />
            <option value="Cabo Verde" />
            <option value="Cambodia" />
            <option value="Cameroon" />
            <option value="Canada" />
            <option value="Central African Republic" />
            <option value="Chad" />
            <option value="Chile" />
            <option value="China" />
            <option value="Colombia" />
            <option value="Comoros" />
            <option value="Congo, Democratic Republic of the" />
            <option value="Congo, Republic of the" />
            <option value="Costa Rica" />
            <option value="Croatia" />
            <option value="Cuba" />
            <option value="Cyprus" />
            <option value="Czech Republic" />
            <option value="Denmark" />
            <option value="Djibouti" />
            <option value="Dominica" />
            <option value="Dominican Republic" />
            <option value="East Timor (Timor-Leste)" />
            <option value="Ecuador" />
            <option value="Egypt" />
            <option value="El Salvador" />
            <option value="Equatorial Guinea" />
            <option value="Eritrea" />
            <option value="Estonia" />
            <option value="Eswatini" />
            <option value="Ethiopia" />
            <option value="Fiji" />
            <option value="Finland" />
            <option value="France" />
            <option value="Gabon" />
            <option value="Gambia" />
            <option value="Georgia" />
            <option value="Germany" />
            <option value="Ghana" />
            <option value="Greece" />
            <option value="Grenada" />
            <option value="Guatemala" />
            <option value="Guinea" />
            <option value="Guinea-Bissau" />
            <option value="Guyana" />
            <option value="Haiti" />
            <option value="Honduras" />
            <option value="Hungary" />
            <option value="Iceland" />
            <option value="India" />
            <option value="Indonesia" />
            <option value="Iran" />
            <option value="Iraq" />
            <option value="Ireland" />
            <option value="Israel" />
            <option value="Italy" />
            <option value="Ivory Coast" />
            <option value="Jamaica" />
            <option value="Japan" />
            <option value="Jordan" />
            <option value="Kazakhstan" />
            <option value="Kenya" />
            <option value="Kiribati" />
            <option value="Korea, North" />
            <option value="Korea, South" />
            <option value="Kosovo" />
            <option value="Kuwait" />
            <option value="Kyrgyzstan" />
            <option value="Laos" />
            <option value="Latvia" />
            <option value="Lebanon" />
            <option value="Lesotho" />
            <option value="Liberia" />
            <option value="Libya" />
            <option value="Liechtenstein" />
            <option value="Lithuania" />
            <option value="Luxembourg" />
            <option value="Madagascar" />
            <option value="Malawi" />
            <option value="Malaysia" />
            <option value="Maldives" />
            <option value="Mali" />
            <option value="Malta" />
            <option value="Marshall Islands" />
            <option value="Mauritania" />
            <option value="Mauritius" />
            <option value="Mexico" />
            <option value="Micronesia" />
            <option value="Moldova" />
            <option value="Monaco" />
            <option value="Mongolia" />
            <option value="Montenegro" />
            <option value="Morocco" />
            <option value="Mozambique" />
            <option value="Myanmar" />
            <option value="Namibia" />
            <option value="Nauru" />
            <option value="Nepal" />
            <option value="Netherlands" />
            <option value="New Zealand" />
            <option value="Nicaragua" />
            <option value="Niger" />
            <option value="Nigeria" />
            <option value="North Macedonia" />
            <option value="Norway" />
            <option value="Oman" />
            <option value="Pakistan" />
            <option value="Palau" />
            <option value="Panama" />
            <option value="Papua New Guinea" />
            <option value="Paraguay" />
            <option value="Peru" />
            <option value="Philippines" />
            <option value="Poland" />
            <option value="Portugal" />
            <option value="Qatar" />
            <option value="Romania" />
            <option value="Russia" />
            <option value="Rwanda" />
            <option value="Saint Kitts and Nevis" />
            <option value="Saint Lucia" />
            <option value="Saint Vincent and the Grenadines" />
            <option value="Samoa" />
            <option value="San Marino" />
            <option value="Sao Tome and Principe" />
            <option value="Saudi Arabia" />
            <option value="Senegal" />
            <option value="Serbia" />
            <option value="Seychelles" />
            <option value="Sierra Leone" />
            <option value="Singapore" />
            <option value="Slovakia" />
            <option value="Slovenia" />
            <option value="Solomon Islands" />
            <option value="Somalia" />
            <option value="South Africa" />
            <option value="South Sudan" />
            <option value="Spain" />
            <option value="Sri Lanka" />
            <option value="Sudan" />
            <option value="Suriname" />
            <option value="Sweden" />
            <option value="Switzerland" />
            <option value="Syria" />
            <option value="Taiwan" />
            <option value="Tajikistan" />
            <option value="Tanzania" />
            <option value="Thailand" />
            <option value="Togo" />
            <option value="Tonga" />
            <option value="Trinidad and Tobago" />
            <option value="Tunisia" />
            <option value="Turkey" />
            <option value="Turkmenistan" />
            <option value="Tuvalu" />
            <option value="Uganda" />
            <option value="Ukraine" />
            <option value="United Arab Emirates" />
            <option value="United Kingdom" />
            <option value="United States" />
            <option value="Uruguay" />
            <option value="Uzbekistan" />
            <option value="Vanuatu" />
            <option value="Vatican City" />
            <option value="Venezuela" />
            <option value="Vietnam" />
            <option value="Yemen" />
            <option value="Zambia" />
            <option value="Zimbabwe" />
          </datalist>
        </div>

        <div className="sm:col-span-2">
          <div>
            <input
              type="text"
              value={addressDetails.pincode}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    pincode: value,
                  }))
                )
              }
              required
              placeholder="pincode"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            ></input>
          </div>
        </div>

        <div className="sm:col-span-3">
          <div className="flex items-center">
            <input
              type="text"
              value={addressDetails.officeTelephone}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    officeTelephone: value,
                  }))
                )
              }
              required
              placeholder="Office Telephone"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            />
            <span className="text-red-500 ml-1">*</span>
          </div>
        </div>

        <div className="sm:col-span-3">
          <div>
            <input
              type="text"
              value={addressDetails.fieldOfActivity}
              onChange={(e) =>
                updateData(e, (value) =>
                  setAddressDetails((prevCompany) => ({
                    ...prevCompany,
                    fieldOfActivity: value,
                  }))
                )
              }
              required
              placeholder="Field of Activity"
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
            ></input>
          </div>
        </div>

        <div className="sm:col-span-2 mt-2 cursor-pointer" onClick={addAddress}>
          <i class="fa fa-plus-square" aria-hidden="true"></i>
        </div>

        <div className="flex items-center">
          <button
            className="bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded-md shadow-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-opacity-50"
            onClick={() => setShowAddressesArray(!showAddressesArray)}
          >
            {showAddressesArray ? "Hide Addresses" : "Show Addresses"}
          </button>
        </div>

        {showAddressesArray && (
          <div className="col-span-full">
            <div className="flex flex-row flex-wrap gap-6 justify-center mt-10">
              {company?.addresses?.map((ele, index) => (
                <div
                  key={index}
                  className="max-w-md bg-white shadow-lg rounded-lg overflow-hidden"
                >
                  <div className="bg-indigo-600 text-white py-4 px-6 rounded-t-lg flex justify-between items-center gap-2">
                    <div
                      className="text-md font-medium truncate"
                      title={ele.companyAddress || "Company Address"}
                    >
                      {ele.companyAddress || "Company Address"}
                    </div>
                    <div className="flex flex-row justify-end items-center gap-2">
                      <div
                        onClick={() => editAddress(index)}
                        className="cursor-pointer"
                      >
                        <i
                          className="fa fa-pencil-square"
                          aria-hidden="true"
                        ></i>
                      </div>
                      {index === indexToUpdate && (
                        <div
                          onClick={() => updateAddress(index)}
                          className="cursor-pointer"
                        >
                          <i className="fa fa-check" aria-hidden="true"></i>
                        </div>
                      )}
                      <div
                        onClick={() => deleteAddress(index)}
                        className="cursor-pointer"
                      >
                        <i className="fa fa-trash" aria-hidden="true"></i>
                      </div>
                      <button
                        onClick={() => showContactPopup(index)}
                        className="bg-white text-indigo-600 py-1 px-3 rounded-full hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                      >
                        <i class="fa fa-user" aria-hidden="true"></i>
                      </button>

                      {contactPopup && (
                        <ContactContext.Provider
                          value={{
                            contactPopup,
                            setContactPopup,
                            addPersonDetails,
                            currentAddressIndex,
                          }}
                        >
                          {" "}
                          <ContactPopup></ContactPopup>{" "}
                        </ContactContext.Provider>
                      )}
                    </div>
                  </div>
                  <div className="px-6 py-4">
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        Company Address:
                      </label>
                      {indexToUpdate === index ? (
                        <input
                          type="text"
                          value={addressDetails.companyAddress}
                          onChange={(e) =>
                            updateData(e, (value) =>
                              setAddressDetails((prevCompany) => ({
                                ...prevCompany,
                                companyAddress: value,
                              }))
                            )
                          }
                          required
                          className="mt-1 block w-full rounded-md border border-gray-300 p-2 text-gray-900 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                        />
                      ) : (
                        <div className="mt-1 text-gray-700">
                          {ele.companyAddress || "-"}
                        </div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        City:
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.city}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  city: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">{ele.city || "-"}</div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        State
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.state}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  state: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">{ele.state || "-"}</div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        Country
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.country}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  country: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">
                          {ele.country || "-"}
                        </div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        Office Telephone
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.officeTelephone}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  officeTelephone: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">
                          {ele.officeTelephone || "-"}
                        </div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        Pincode
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.pincode}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  pincode: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">
                          {ele.pincode || "-"}
                        </div>
                      )}
                    </div>
                    <div className="mb-4">
                      <label className="block text-gray-700 font-medium mb-1">
                        Field Of Activity
                      </label>
                      {indexToUpdate === index ? (
                        <div className="text-gray-600">
                          <input
                            type="text"
                            value={addressDetails.fieldOfActivity}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setAddressDetails((prevCompany) => ({
                                  ...prevCompany,
                                  fieldOfActivity: value,
                                }))
                              )
                            }
                            required
                            className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                          ></input>
                        </div>
                      ) : (
                        <div className="text-gray-600">
                          {ele.fieldOfActivity || "-"}
                        </div>
                      )}
                    </div>

                    {company.addresses[index].contacts.map(
                      (contact, contactIndex) => (
                        <button
                          key={contactIndex}
                          onClick={() => handleShowContact(index, contactIndex)}
                          className="bg-gray-200 p-2 rounded-md m-1 hover:bg-gray-300"
                        >
                          {contact.salutation} {contact.contactPersonName}
                        </button>
                      )
                    )}
                    {isContactModalOpen && (
                      <div className="fixed inset-0 bg-gray-900 bg-opacity-50 flex items-center justify-center z-50">
                        <div className="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full p-6">
                          <div className="flex flex-row justify-between items-center bg-blue-600 text-white rounded-t-md py-2 px-4 mb-4">
                            <div className="text-lg font-medium">
                              {contactPersonDetail.salutation +
                                " " +
                                contactPersonDetail.contactPersonName}
                            </div>
                            <div className="flex flex-row justify-end gap-2">
                              <div
                                onClick={() =>
                                  editAddressContact(index, currentContactIndex)
                                }
                              >
                                <i
                                  className="fa fa-pencil-square"
                                  aria-hidden="true"
                                ></i>
                              </div>
                              {editContact && (
                                <div
                                  onClick={() =>
                                    updateAddressContact(
                                      index,
                                      currentContactIndex
                                    )
                                  }
                                >
                                  <i
                                    className="fa fa-eject"
                                    aria-hidden="true"
                                  ></i>
                                </div>
                              )}
                              <div
                                onClick={() =>
                                  deleteAddressContact(index, contactIndex)
                                }
                              >
                                <i
                                  className="fa fa-trash"
                                  aria-hidden="true"
                                ></i>
                              </div>
                            </div>
                          </div>
                          <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Type Of Business
                              </div>
                              {editContact ? (
                                <div className="text-gray-600">
                                  <input
                                    type="text"
                                    value={contactPersonDetail.typeOfBusiness}
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            typeOfBusiness: value,
                                          })
                                        )
                                      )
                                    }
                                    list="typeOfBusiness"
                                    required
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                  <datalist id="typeOfBusiness">
                                    <option value="TM"></option>
                                    <option value="Patent"></option>
                                    <option value="Copyright"></option>
                                    <option value="Design"></option>
                                    <option value="Litigation"></option>
                                  </datalist>
                                </div>
                              ) : (
                                <div className="text-gray-600">
                                  {contactPersonDetail.typeOfBusiness}
                                </div>
                              )}
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Contact Person Name
                              </div>
                              {editContact ? (
                                <div className="text-gray-600">
                                  <input
                                    type="text"
                                    value={contactPersonDetail.salutation}
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            salutation: value,
                                          })
                                        )
                                      )
                                    }
                                    list="salutations"
                                    required
                                    className="block w-20 rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                  <datalist id="salutations">
                                    <option value="Mr."></option>
                                    <option value="Ms."></option>
                                    <option value="Mrs."></option>
                                    <option value="Miss"></option>
                                    <option value="Dr."></option>
                                    <option value="Prof."></option>
                                  </datalist>
                                  <input
                                    type="text"
                                    value={
                                      contactPersonDetail.contactPersonName
                                    }
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            contactPersonName: value,
                                          })
                                        )
                                      )
                                    }
                                    required
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                </div>
                              ) : (
                                <div className="text-gray-600">
                                  {contactPersonDetail.salutation +
                                    " " +
                                    contactPersonDetail.contactPersonName}
                                </div>
                              )}
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Designation
                              </div>
                              {editContact ? (
                                <div className="text-gray-600">
                                  <input
                                    type="text"
                                    value={contactPersonDetail.designation}
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            designation: value,
                                          })
                                        )
                                      )
                                    }
                                    required
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                </div>
                              ) : (
                                <div className="text-gray-600">
                                  {contactPersonDetail.designation}
                                </div>
                              )}
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Department
                              </div>
                              <div className="text-gray-600">
                                {editContact ? (
                                  <div className="text-gray-600">
                                    <input
                                      type="text"
                                      value={contactPersonDetail.department}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setContactPersonDetails(
                                            (prevDetails) => ({
                                              ...prevDetails,
                                              department: value,
                                            })
                                          )
                                        )
                                      }
                                      required
                                      className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                    />
                                  </div>
                                ) : (
                                  <div className="text-gray-600">
                                    {contactPersonDetail.department}
                                  </div>
                                )}
                              </div>
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Contact Person Email
                              </div>
                              {editContact ? (
                                <div className="text-gray-600">
                                  <input
                                    type="text"
                                    value={contactPersonDetail.email}
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            email: value,
                                          })
                                        )
                                      )
                                    }
                                    required
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                </div>
                              ) : (
                                <div className="text-gray-600">
                                  {contactPersonDetail.email}
                                </div>
                              )}
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Official Mobile Number
                              </div>
                              <div className="text-gray-600">
                                {editContact ? (
                                  <div className="text-gray-600">
                                    <input
                                      type="text"
                                      value={contactPersonDetail.officialMobile}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setContactPersonDetails(
                                            (prevDetails) => ({
                                              ...prevDetails,
                                              officialMobile: value,
                                            })
                                          )
                                        )
                                      }
                                      required
                                      className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                    />
                                  </div>
                                ) : (
                                  <div className="text-gray-600">
                                    {contactPersonDetail.officialMobile}
                                  </div>
                                )}
                              </div>
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Personal Mobile Number
                              </div>
                              {editContact ? (
                                <div className="text-gray-600">
                                  <input
                                    type="text"
                                    value={contactPersonDetail.personalMobile}
                                    onChange={(e) =>
                                      updateData(e, (value) =>
                                        setContactPersonDetails(
                                          (prevDetails) => ({
                                            ...prevDetails,
                                            personalMobile: value,
                                          })
                                        )
                                      )
                                    }
                                    required
                                    className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                  />
                                </div>
                              ) : (
                                <div className="text-gray-600">
                                  {contactPersonDetail.personalMobile}
                                </div>
                              )}
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                LinkedIn Profile
                              </div>
                              <div className="text-gray-600">
                                {editContact ? (
                                  <div className="text-gray-600">
                                    <input
                                      type="text"
                                      value={contactPersonDetail.linkdinProfile}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setContactPersonDetails(
                                            (prevDetails) => ({
                                              ...prevDetails,
                                              linkdinProfile: value,
                                            })
                                          )
                                        )
                                      }
                                      required
                                      className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                    />
                                  </div>
                                ) : (
                                  <div className="text-gray-600">
                                    {contactPersonDetail.linkdinProfile}
                                  </div>
                                )}
                              </div>
                            </div>
                            <div className="bg-gray-200 p-4 border rounded-md">
                              <div className="block text-sm font-medium leading-6 text-gray-900">
                                Remarks
                              </div>
                              <div className="text-gray-600">
                                {editContact ? (
                                  <div className="text-gray-600">
                                    <input
                                      type="text"
                                      value={contactPersonDetail.remarks}
                                      onChange={(e) =>
                                        updateData(e, (value) =>
                                          setContactPersonDetails(
                                            (prevDetails) => ({
                                              ...prevDetails,
                                              remarks: value,
                                            })
                                          )
                                        )
                                      }
                                      required
                                      className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                    />
                                  </div>
                                ) : (
                                  <div className="text-gray-600">
                                    {contactPersonDetail.remarks}
                                  </div>
                                )}
                              </div>
                            </div>
                            <div className="col-span-full sm:col-span-2">
                              <button
                                onClick={handleCloseContact}
                                className="mt-4 bg-red-600 hover:bg-red-700 text-white py-2 px-4 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 ml-4"
                              >
                                Close
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default AddressComponent;
