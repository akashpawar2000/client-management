import React, { useContext, useEffect, useState } from "react";
import PopupContext from "../context/popupContext.js";
import ContactContext from "../context/contactContext.js";
function ContactPopup() {
  const {
    company,
    setCompany,
    updateData,
    contactPersonDetail,
    setContactPersonDetails,
    setCompanyEmpty,
    addressDetails,
    setAddressDetails,
    mode,
    setMode,
  } = useContext(PopupContext);
  const {
    contactPopup,
    setContactPopup,
    addPersonDetails,
    currentAddressIndex,
  } = useContext(ContactContext);

  return (
    <div className="fixed inset-0 bg-gray-900 bg-opacity-50 flex items-center justify-center z-50">
      <div className="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full p-6">
        <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
          <div className="bg-gray-200 p-4 border rounded-md sm:col-span-2">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Contact Person Name <span className="text-red-500 ml-1">*</span>
            </div>
            <div className="flex flex-row">
              <input
                type="text"
                value={contactPersonDetail.salutation}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setContactPersonDetails((prevDetails) => ({
                      ...prevDetails,
                      salutation: value,
                    }))
                  )
                }
                list="salutations"
                required
                className="block w-20 rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              />
              <datalist id="salutations">
                <option value="Mr."></option>
                <option value="Ms."></option>
                <option value="Mrs."></option>
                <option value="Miss"></option>
                <option value="Dr."></option>
                <option value="Prof."></option>
              </datalist>
              <input
                type="text"
                value={contactPersonDetail.contactPersonName}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setContactPersonDetails((prevDetails) => ({
                      ...prevDetails,
                      contactPersonName: value,
                    }))
                  )
                }
                placeholder="FirstName Middlename Surname"
                required
                className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              />{" "}
            </div>
          </div>
          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Type Of Business<span className="text-red-500 ml-1">*</span>
            </div>
            <input
              type="text"
              value={contactPersonDetail.typeOfBusiness}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    typeOfBusiness: value,
                  }))
                )
              }
              list="typeOfBusiness"
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter type of business"
            />
            <datalist id="typeOfBusiness">
              <option value="TM"></option>
              <option value="Patent"></option>
              <option value="Copyright"></option>
              <option value="Design"></option>
              <option value="Litigation"></option>
            </datalist>
          </div>
          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Designation<span className="text-red-500 ml-1">*</span>
            </div>
            <input
              type="text"
              value={contactPersonDetail.designation}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    designation: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter designation"
            />
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Department<span className="text-red-500 ml-1">*</span>
            </div>
            <input
              type="text"
              value={contactPersonDetail.department}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    department: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter department"
            />
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Contact Person Email<span className="text-red-500 ml-1">*</span>
            </div>
            <input
              type="text"
              value={contactPersonDetail.email}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    email: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter email"
            />
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Official Mobile Number{" "}
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              type="text"
              value={contactPersonDetail.officialMobile}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    officialMobile: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter official mobile number"
            />{" "}
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Personal Mobile Number
            </div>
            <input
              type="text"
              value={contactPersonDetail.personalMobile}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    personalMobile: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter personal mobile number"
            />
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              LinkedIn Profile
            </div>
            <input
              type="text"
              value={contactPersonDetail.linkdinProfile}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    linkdinProfile: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter LinkedIn profile URL"
            />
          </div>

          <div className="bg-gray-200 p-4 border rounded-md">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Remarks
            </div>
            <textarea
              rows={3}
              columns={8}
              type="text"
              value={contactPersonDetail.remarks}
              onChange={(e) =>
                updateData(e, (value) =>
                  setContactPersonDetails((prevDetails) => ({
                    ...prevDetails,
                    remarks: value,
                  }))
                )
              }
              required
              className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
              placeholder="Enter remarks"
            />
          </div>

          <div className="col-span-full sm:col-span-2 flex flex-row gap-2">
            <button
              onClick={() => addPersonDetails(currentAddressIndex)}
              className="mt-4 bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-4 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Add Contact Person
            </button>
            <button
              onClick={() => setContactPopup(false)}
              className="mt-4 bg-red-600 hover:bg-red-700 text-white py-2 px-4 rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ContactPopup;
