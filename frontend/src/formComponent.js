import React, { useContext, useEffect, useState } from "react";
import PopupContext from "./context/popupContext.js";
import AddressComponenet from "./addPopupComponenets/addressComponenet.js";
function FormComponent() {
  const {
    company,
    setCompany,
    setAddPopup,
    updateData,
    setCompanyEmpty,
    showUpdate,
    setShowUpdate,
    setShowStage,
  } = useContext(PopupContext);
  const closeAddPopup = () => {
    setAddPopup(false);
    setShowUpdate(false);
    setShowStage(true);
  };

  return (
    <div className="popup">
      <div className="popup-content bg-white shadow-md rounded-md p-4 w-full">
        <div className="popup-header relative mb-4">
          <div className="pop-header text-3xl font-semibold bg-yellow-300 p-4 rounded-lg text-center">
            Company Details
          </div>
          <span
            className="close cursor-pointer absolute top-0 right-0 p-3 text-xl"
            onClick={closeAddPopup}
          >
            &times;
          </span>
        </div>

        <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-2">
          <div className="col-span-full">
            <div className="block text-sm font-medium leading-6 text-gray-900">
              Name of Company <span className="text-red-500">*</span>
            </div>
            <div className="mt-2">
              <input
                type="text"
                value={company.nameOfCompany}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setCompany((prevCompany) => ({
                      ...prevCompany,
                      nameOfCompany: value,
                    }))
                  )
                }
                required
                className="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              ></input>
            </div>
          </div>

          <div class="col-span-full">
            <div class="block text-sm font-medium leading-6 text-gray-900">
              Company website <span className="text-red-500">*</span>
            </div>
            <div class="mt-1">
              <input
                type="text"
                value={company.companyWebsite}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setCompany((prevCompany) => ({
                      ...prevCompany,
                      companyWebsite: value,
                    }))
                  )
                }
                required
                class="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              ></input>
            </div>
          </div>

          <div class="col-span-full">
            <div class="block text-sm font-medium leading-6 text-gray-900">
              LinkedIn profile of company{" "}
            </div>
            <div class="mt-1">
              <input
                type="text"
                value={company.linkedInProfileOfCompany}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setCompany((prevCompany) => ({
                      ...prevCompany,
                      linkedInProfileOfCompany: value,
                    }))
                  )
                }
                required
                class="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              ></input>
            </div>
          </div>

          <div class="col-span-full">
            <div class="block text-sm font-medium leading-6 text-gray-900">
              Category <span className="text-red-500">*</span>
            </div>
            <div class="mt-1">
              <input
                type="text"
                value={company.category}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setCompany((prevCompany) => ({
                      ...prevCompany,
                      category: value,
                    }))
                  )
                }
                required
                list="category"
                class="block w-full rounded-md border-0 p-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
              ></input>
              <datalist id="category">
                <option value="Indian client"></option>
                <option value="Foreign client"></option>
                <option value="Foreign law firm"></option>
                <option value="Indian law firm"></option>
                <option value="CA firm"></option>
                <option value="Adv. Agency"></option>
                <option value="Vendor"></option>
                <option value="Service provider"></option>
              </datalist>
            </div>
          </div>
        </div>

        <AddressComponenet></AddressComponenet>
      </div>
    </div>
  );
}

export default React.memo(FormComponent);
