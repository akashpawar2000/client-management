import React, { useState, useEffect, useContext } from "react";
import PopupContext from "../context/popupContext.js";
function StaticContent1(props) {
  const [stageCount, setStageCount] = useState([]);
  const [totalApplicants, setTotalApplicants] = useState("");

  const { patent, patents, addPopup, handleTotalSearch, handleSearchStatus } =
    useContext(PopupContext);

  // This hook is used to count no of applications in each stage like {STATUS: 'Written Submission Filed', statusCount: 1}
  useEffect(() => {
    fetch("http://192.168.1.191:5000/api/statuscount")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .then((data) => {
        // Do something with the data received from the server
        console.log("data", data);
        setStageCount((prev) => [...prev, ...data]);
      })
      .catch((error) => {
        console.error("There was a problem with your fetch operation:", error);
      });
  }, []);

  //Hook to calculate count of total applicants
  useEffect(() => {
    fetch(`http://192.168.1.191:5000/api/totalpatent`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network Error");
        }
        console.log(response);
        return response.json();
      })
      .then((data) => {
        console.log(data.count);
        setTotalApplicants(data.count);
      })
      .catch((error) => {
        console.error("Fetch error:", error);
      });
  }, [addPopup]);

  return (
    <div className="staticContent1 flex flex-wrap bg-gray-100 p-6 rounded-lg shadow-md">
      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <label
          className="cursor-pointer text-blue-500 font-bold"
          onClick={handleTotalSearch}
        >
          Total applications:
        </label>
        <div className="text-lg font-semibold">{totalApplicants}</div>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Application Filed/Application not published");
          }}
        >
          Application Filed/Application not published/Request For Examination
          not filed:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) =>
                STATUS === "Application Filed/Application not published"
            )?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus(
              "Application Published/Request for Examination not Filed"
            );
          }}
        >
          Application Published/Request for Examination not Filed:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) =>
                STATUS ===
                "Application Published/Request for Examination not Filed"
            )?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus(
              "Request For Examination filed/Application Not published"
            );
          }}
        >
          Request For Examination filed/Application Not published:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) =>
                STATUS ===
                "Request For Examination filed/Application Not published"
            )?.statusCount
          }
        </span>
      </div>
      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("FER Not issued");
          }}
        >
          FER Not issued:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "FER Not issued")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("FER issued/FER Reply not filed");
          }}
        >
          FER issued/FER Reply not filed:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) => STATUS === "FER issued/FER Reply not filed"
            )?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("FER Replied");
          }}
        >
          FER Replied:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "FER Replied")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Hearing Scheduled");
          }}
        >
          Hearing Scheduled:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "Hearing Scheduled")
              ?.statusCount
          }
        </span>
      </div>

      {/* Repeat for other div elements */}
      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Written Submission Filed");
          }}
        >
          Written Submission Filed:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) => STATUS === "Written Submission Filed"
            )?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Abandoned");
          }}
        >
          Abandoned:{" "}
        </span>
        <span className="text-lg font-semibold">
          {stageCount.find(({ STATUS }) => STATUS === "Abandoned")?.statusCount}
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Deemed to be Withdrawn");
          }}
        >
          Deemed to be withdrawn:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "Deemed to be withdrawn")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Deemed to be abandoned");
          }}
        >
          Deemed to be abandoned:{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "Deemed to be abandoned")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Application Withdrawn");
          }}
        >
          Application Withdrawn :{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "Application Withdrawn")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("AWAITING NBA APPROVAL");
          }}
        >
          Awaiting NBA Approval :{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(({ STATUS }) => STATUS === "AWAITING NBA APPROVAL")
              ?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("AWAITING APPROVAL FROM DRDO/DAE");
          }}
        >
          Awaiting Approval From DRDO/DAE :{" "}
        </span>
        <span className="text-lg font-semibold">
          {
            stageCount.find(
              ({ STATUS }) => STATUS === "AWAITING APPROVAL FROM DRDO/DAE"
            )?.statusCount
          }
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Granted");
          }}
        >
          Granted:{" "}
        </span>
        <span className="text-lg font-semibold">
          {stageCount.find(({ STATUS }) => STATUS === "Granted")?.statusCount}
        </span>
      </div>

      <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg shadow-md mr-4 mb-4">
        <span
          className="cursor-pointer text-blue-500 font-bold"
          onClick={() => {
            handleSearchStatus("Rejected");
          }}
        >
          Rejected:{" "}
        </span>
        <span className="text-lg font-semibold">
          {stageCount.find(({ STATUS }) => STATUS === "Rejected")?.statusCount}
        </span>
      </div>
    </div>
  );
}
export default React.memo(StaticContent1);
