function StaticContent2() {
  return (
    <div className="staticContent2 flex flex-wrap p-4 bg-gray-100 justify-between">
      <div className="section flex items-center mr-4 mb-4">
        <div className="input-group">
          <button
            className="button bg-blue-500 text-white px-4 py-2 rounded-md"
            onClick={handleSearchButton}
          >
            Search Application No
          </button>
        </div>
      </div>
      <div className="section flex items-center mr-4 mb-4">
        <button
          className="button bg-green-500 text-white px-4 py-2 rounded-md"
          onClick={showAdd}
        >
          Click here to add new clients
        </button>
      </div>
      <div className="section flex items-center mr-4 mb-4 relative">
        <div className="relative">
          <div className="inline-block relative">
            <button
              className="bg-white border border-gray-300 rounded-md px-4 py-2 inline-flex items-center"
              onClick={toggleDropdown}
              aria-expanded={isOpen ? "true" : "false"}
            >
              <span className="mr-2 text-gray-400">
                {searchName ? searchName : "Enter Applicant Name"}{" "}
              </span>

              {searchName && (
                <span
                  className="text-gray-600 cursor-pointer"
                  onClick={() => setSearchName("")}
                >
                  &times;
                </span>
              )}
              <svg
                className="fill-current h-4 w-4 ml-2"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path fillRule="evenodd" d="M10 12a2 2 0 100-4 2 2 0 000 4z" />
              </svg>
            </button>
            {isOpen && (
              <div className="absolute right-0 mt-2 w-70 max-h-60 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 overflow-y-auto z-10">
                <div className="p-2">
                  <input
                    type="text"
                    value={searchName}
                    onChange={handleSearchChange}
                    placeholder="Search"
                    className="border border-gray-300 rounded-md px-3 py-2 w-full mb-2"
                  />
                  <ul>
                    {searchName === ""
                      ? clients.sort().map((name, index) => (
                          <li
                            key={index}
                            onClick={() => handleSearchApplicantName(name)}
                            className="px-3 py-2 cursor-pointer hover:bg-gray-100"
                          >
                            {name}
                          </li>
                        ))
                      : clients
                          .sort()
                          .filter((name) =>
                            name
                              .toLowerCase()
                              .includes(searchName.toLowerCase())
                          )
                          .map((name, index) => (
                            <li
                              key={index}
                              onClick={() => handleSearchApplicantName(name)}
                              className="px-3 py-2 cursor-pointer hover:bg-gray-100"
                            >
                              {name}
                            </li>
                          ))}
                  </ul>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default React.memo(StaticContent2);
