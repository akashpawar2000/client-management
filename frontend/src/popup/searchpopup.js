import React, { useState, useContext } from "react";
import PopupContext from "../context/popupContext.js";
function SearchPopup() {
  const {
    patent,
    setPatent,
    setShowSearch,
    remark,
    setRemark,
    addRemark,
    mail,
    setMail,
    addMail,
    updateData,
    handleFileChange,
  } = useContext(PopupContext);
  const closeSearchPopup = () => {
    setPatent({
      APPLICATION_NUMBER: "",
      APPLICATION_TYPE: "",
      APPLICANT_TYPE: "",
      JURISDICTION: "",
      applicants: [],
      inventors: [],
      Group: "",
      DATE_OF_FILING: "",
      TITLE_OF_INVENTION: "",
      DUE_DATE_OF_PUBLICATION: "",
      DATE_OF_GRANT: "",
      PUBLICATION_DATE_U_S_11A: "",
      REQUEST_FOR_EXAMINATION_DATE: "",
      FER_Issue_Date: "",
      DUE_DATE_FOR_FER_Reply_Date: "",
      FER_Reply_Date: "",
      Hearing_Date: "",
      WS_filing_date: "",
      IPO_APPLICATION_STATUS: "",
      STATUS: "",
      Granted_or_Rejected: "",
      Controller_Name: "",
      Controller_Reply: "",
      mails: [], //mail
      remarks: [], //association
      CLIENT_RESPONSE: "",
      file: [],
    });
    setSearchApplicationNo("");
    setShowSearch(false);
  };

  const [searchApplicationNo, setSearchApplicationNo] = useState("");

  const handleSearch = () => {
    if (searchApplicationNo.trim() === "") {
      alert("Application No cant't be empty");
    } else {
      fetch(
        `http://192.168.1.191:5000/api/patent/${encodeURIComponent(
          searchApplicationNo
        )}`
      ) //chanage riddhi
        .then((response) => {
          if (!response.ok) {
            throw new Error("Network Error");
          }
          console.log("response object", response);
          return response.json();
        })
        .then((data) => {
          console.log("dp=dsfsfta", data);
          console.log("dp=dsfsfta", data.application.file);
          console.log("dp=dsfsfta1111", data.application.files);

          setSearchApplicationNo("");
          setPatent(data.application);
        })
        .catch((error) => {
          console.error("Fetch error:", error);
          alert(
            "Application not found, Please Enter correct Application Number"
          );
          setPatent({
            APPLICATION_NUMBER: "",
            APPLICATION_TYPE: "",
            APPLICANT_TYPE: "",
            JURISDICTION: "",
            applicants: [],
            inventors: [],
            Group: "",
            DATE_OF_FILING: "",
            TITLE_OF_INVENTION: "",
            DUE_DATE_OF_PUBLICATION: "",
            DATE_OF_GRANT: "",
            PUBLICATION_DATE_U_S_11A: "",
            REQUEST_FOR_EXAMINATION_DATE: "",
            FER_Issue_Date: "",
            DUE_DATE_FOR_FER_Reply_Date: "",
            FER_Reply_Date: "",
            Hearing_Date: "",
            WS_filing_date: "",
            IPO_APPLICATION_STATUS: "",
            STATUS: "",
            Granted_or_Rejected: "",
            Controller_Name: "",
            Controller_Reply: "",
            mails: [], //mail
            remarks: [], //association
            CLIENT_RESPONSE: "",
            file: [],
          });
        });
    }
  };

  const handleSave = () => {
    handleSubmit();
  };

  //this is for update through search application no popup
  const handleSubmit = async () => {
    try {
      const response = await fetch(
        "http://192.168.1.191:5000/api/updatepatent",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(patent),
        }
      );

      if (!response.ok) {
        throw new Error("Failed to submit patent data");
      }
      setPatent({
        APPLICATION_NUMBER: "",
        APPLICATION_TYPE: "",
        APPLICANT_TYPE: "",
        JURISDICTION: "",
        applicants: [],
        inventors: [],
        Group: "",
        DATE_OF_FILING: "",
        TITLE_OF_INVENTION: "",
        DUE_DATE_OF_PUBLICATION: "",
        DATE_OF_GRANT: "",
        PUBLICATION_DATE_U_S_11A: "",
        REQUEST_FOR_EXAMINATION_DATE: "",
        FER_Issue_Date: "",
        DUE_DATE_FOR_FER_Reply_Date: "",
        FER_Reply_Date: "",
        Hearing_Date: "",
        WS_filing_date: "",
        IPO_APPLICATION_STATUS: "",
        STATUS: "",
        Granted_or_Rejected: "",
        Controller_Name: "",
        Controller_Reply: "",
        mails: [], //mail
        remarks: [], //association
        CLIENT_RESPONSE: "",
        file: [],
      });

      alert("Data Updated Sucessfully");
    } catch (error) {
      // Handle errors
      console.error("Error submitting patent data:", error.message);
    }
  };
  return (
    <div className="popup">
      <div className="popup-content bg-white shadow-md rounded-md p-4">
        <div className="popup-header relative mb-4">
          <div className="pop-header text-3xl font-semibold bg-yellow-300 p-4 rounded-lg text-center">
            Application Search
          </div>
          <span
            className="close cursor-pointer absolute top-0 right-0 p-3 text-xl"
            onClick={closeSearchPopup}
          >
            &times;
          </span>
        </div>
        <div className="popup-search flex items-center mb-4">
          <label className="mr-2">Application No:</label>
          <input
            type="text"
            value={searchApplicationNo}
            onChange={(e) => setSearchApplicationNo(e.target.value)}
            className="border rounded-md p-1 mr-2"
          />
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded"
            onClick={handleSearch}
          >
            Search
          </button>
          <span>
            {/* <button
                disabled={!edit}
                onClick={handleButtonClick}
                className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-2 rounded"
              >
                Edit
              </button> */}
          </span>
        </div>
        {/* I want it in for colums like 1 colums application no 2               <b>{patent.APPLICATION_NUMBER}</b> 3 patent office 4        <b> {patent.JURISDICTION}</b> AND SO ON FOR ALL DATA */}
        <div class="grid grid-cols-4 gap-4">
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Application No</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.APPLICATION_NUMBER}
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold"> Status </div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {" "}
              {patent.STATUS}
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Patent Office</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.JURISDICTION}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Applicant Name:</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.applicants.map(({ APPLICANT_NAME }) => (
                <div key={APPLICANT_NAME}>{APPLICANT_NAME}</div>
              ))}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Inventor Name:</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.inventors.map(({ INVENTOR_NAME }) => (
                <div key={INVENTOR_NAME}>{INVENTOR_NAME}</div>
              ))}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Application Type:</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.APPLICATION_TYPE}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Applicant Type:</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.APPLICANT_TYPE}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Title of Invention</div>
            <div class="mt-1 w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500">
              {patent.TITLE_OF_INVENTION}
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Date of Filing:</div>
            <div class="mt-1 ">
              <input
                type="date"
                value={patent.DATE_OF_FILING}
                disabled
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Due Date of Publication:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.DUE_DATE_OF_PUBLICATION}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      DUE_DATE_OF_PUBLICATION: value,
                    }))
                  )
                }
                disabled
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Publication Date:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.PUBLICATION_DATE_U_S_11A}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      PUBLICATION_DATE_U_S_11A: value,
                    }))
                  )
                }
                disabled={patent.DATE_OF_FILING.trim() === "" ? true : false}
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">
              Request for Examination Date:
            </div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.REQUEST_FOR_EXAMINATION_DATE}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      REQUEST_FOR_EXAMINATION_DATE: value,
                    }))
                  )
                }
                disabled={patent.DATE_OF_FILING.trim() === "" ? true : false}
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">FER Issue Date:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.FER_Issue_Date}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      FER_Issue_Date: value,
                    }))
                  )
                }
                disabled={
                  patent.REQUEST_FOR_EXAMINATION_DATE.trim() === ""
                    ? true
                    : false
                }
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Due Date for FER Reply:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.DUE_DATE_FOR_FER_Reply_Date}
                disabled
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">FER Reply Date:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.FER_Reply_Date}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      FER_Reply_Date: value,
                    }))
                  )
                }
                disabled={patent.FER_Issue_Date.trim() === "" ? true : false}
              />
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Hearing Date:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.Hearing_Date}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      Hearing_Date: value,
                    }))
                  )
                }
                disabled={patent.FER_Reply_Date.trim() === "" ? true : false}
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">WS Filing Date:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.WS_filing_date}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      WS_filing_date: value,
                    }))
                  )
                }
                disabled={patent.Hearing_Date.trim() === "" ? true : false}
              />
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">IPO Application Status:</div>
            <div class="mt-1">
              <select
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.IPO_APPLICATION_STATUS}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      IPO_APPLICATION_STATUS: value,
                    }))
                  )
                }
              >
                <option value="">Select</option>
                <option value="Abandoned U/s 21(1)">Abandoned U/s 21(1)</option>
                <option value="Application Awaiting Examination">
                  Application Awaiting Examination
                </option>
                <option value="Application Filed">Application Filed</option>
                <option value="Application in Hearing">
                  Application in Hearing
                </option>
                <option value="Application not published">
                  Application not published
                </option>
                <option value="Application referred u/s 12 for examination">
                  Application referred u/s 12 for examination
                </option>
                <option value="Application Refused U/S 15">
                  Application Refused U/S 15
                </option>
                <option value="Reply Filed. Application in amended examination">
                  Reply Filed. Application in amended examination
                </option>
                <option value="Application Withdrawn">
                  Application Withdrawn
                </option>
                <option value="Awaiting Request for Examination">
                  Awaiting Request for Examination
                </option>
                <option value="FER Issued, Reply not Filed">
                  FER Issued, Reply not Filed
                </option>
                <option value="Granted Application">Granted Application</option>
                <option value="Reply not Filed Deemed to be abandoned U/s 21(1)">
                  Reply not Filed Deemed to be abandoned U/s 21(1)
                </option>
                <option value="Withdrawn Under Section 11B(4){1}">
                  Withdrawn Under Section 11B(4){1}
                </option>
                <option value="Written submission submitted on 14/2/24">
                  Written submission filed
                </option>
              </select>
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Awaiting approvals:</div>
            <div class="mt-1">
              <select
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.Granted_or_Rejected}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      Granted_or_Rejected: value,
                    }))
                  )
                }
              >
                <option value="AWAITING NBA APPROVAL">
                  AWAITING NBA APPROVAL
                </option>
                <option value="AWAITING APPROVAL FROM DRDO/DAE">
                  AWAITING APPROVAL FROM DRDO/DAE
                </option>
              </select>
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Granted or Rejected:</div>
            <div class="mt-1">
              <select
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.Granted_or_Rejected}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      Granted_or_Rejected: value,
                    }))
                  )
                }
              >
                <option value="Pending">Pending</option>
                <option value="Granted">Granted</option>
                <option value="Rejected">Rejected</option>
                <option value="Abandoned">Abandoned</option>
                <option value="Deemed to be withdrawn">
                  Deemed to be withdrawn
                </option>
                <option value="Deemed to be abandoned">
                  Deemed to be abandoned
                </option>
                <option value="Application Withdrawn">
                  Application Withdrawn
                </option>
              </select>
            </div>
          </div>

          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Date Of Grant:</div>
            <div class="mt-1">
              <input
                type="date"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.DATE_OF_GRANT}
                disabled={
                  patent.Granted_or_Rejected != "Granted" ? true : false
                }
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      DATE_OF_GRANT: value,
                    }))
                  )
                }
              />
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold">Patent No</div>
            <div class="mt-1">
              <input
                type="text"
                class="w-full py-3 px-4 border border-gray-300 rounded-md focus:outline-none focus:ring focus:border-blue-500"
                value={patent.patentNo}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      patentNo: value,
                    }))
                  )
                }
                disabled={
                  patent.Granted_or_Rejected.trim() === "" ? true : false
                }
              />
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold"> Controller Name:</div>
            <div class="mt-1">
              {" "}
              <input
                type="text"
                value={patent.Controller_Name}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      Controller_Name: value,
                    }))
                  )
                }
              ></input>
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold"> Controller Reply:</div>
            <div class="mt-1">
              {" "}
              <input
                type="text"
                value={patent.Controller_Reply}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      Controller_Reply: value,
                    }))
                  )
                }
              ></input>
            </div>
          </div>
          <div class="col-span-1 p-4 border border-solid border-gray-400">
            <div class="text-lg font-semibold"> Client Response</div>
            <div class="mt-1">
              {" "}
              <select
                value={patent.CLIENT_RESPONSE}
                onChange={(e) =>
                  updateData(e, (value) =>
                    setPatent((prevPatent) => ({
                      ...prevPatent,
                      CLIENT_RESPONSE: value,
                    }))
                  )
                }
              >
                <option value="interested" selected>
                  Interested
                </option>
                <option value="not interested">Not Interested</option>
                <option value="not interested">Not Responding</option>
              </select>{" "}
            </div>
          </div>
        </div>
        <div class="col-span-1 p-4 border border-solid border-gray-400 mt-5">
          <div class="text-lg font-semibold">File:</div>
          <div class="mt-1">
            <input
              type="file"
              accept=".pdf"
              onChange={handleFileChange}
              multiple
              className="border-b-2 border-gray-300 focus:outline-none focus:border-blue-500"
            />
            {patent.file.map(({ name, data }, index) => (
              <React.Fragment key={index}>
                {data.startsWith("data:application/pdf;base64,") ? (
                  <a
                    href={data}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="underline text-blue-500"
                  >
                    {index + 1}) {name}
                  </a>
                ) : (
                  <a
                    href={`data:application/pdf;base64,${data}`}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="underline text-blue-500"
                  >
                    {index + 1}) {name}
                  </a>
                )}
                &nbsp; &nbsp; &nbsp;
              </React.Fragment>
            ))}
          </div>
        </div>
        {/* <div class="col-span-1 p-4 border border-solid border-gray-400"> */}
        <div className="remark mt-10 ">
          <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
            {" "}
            RKD remark:
          </div>
          <div className="rkd-remark-content p-4 border border-solid border-gray-400">
            <table className="border-collapse border border-gray-300 w-full">
              <tbody>
                <tr>
                  <td className="border border-gray-300 px-4 py-2">
                    <input
                      type="text"
                      value={remark.Remark}
                      placeholder="Write Remark..."
                      onChange={(e) =>
                        updateData(e, (value) =>
                          setRemark((prevRemark) => ({
                            ...prevRemark,
                            Remark: value,
                          }))
                        )
                      }
                      className="w-full focus:outline-none py-1 px-2"
                    />
                  </td>
                  <td className="border border-gray-300 px-4 py-2 text-center">
                    <button
                      className={`add-btn py-1 px-2 rounded bg-blue-500 text-white 
                        }`}
                      onClick={addRemark}
                    >
                      Add
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
            {patent.remarks.length > 0 ? (
              <table className="border-collapse border border-gray-300 mt-4 w-full">
                <thead>
                  <tr>
                    <th className="border border-gray-300 px-4 py-2">No</th>
                    <th className="border border-gray-300 px-4 py-2">Date</th>
                    <th className="border border-gray-300 px-4 py-2">Remark</th>
                  </tr>
                </thead>
                <tbody>
                  {patent.remarks.map(({ Date, Remark }, index) => {
                    return (
                      <tr key={index}>
                        <td className="border border-gray-300 px-4 py-2 text-center">
                          {index + 1}
                        </td>
                        <td className="border border-gray-300 px-4 py-2 text-center">
                          {Date}
                        </td>
                        <td className="border border-gray-300 px-4 py-2 text-center">
                          {Remark}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            ) : (
              <></>
            )}
          </div>
        </div>

        <div className="mail mt-10">
          <div className="rkd-remark-label text-lg font-semibold text-center text-blue-500">
            Follow up Mail Log
          </div>

          <div className="data-date-date overflow-x-auto p-4 border border-solid border-gray-400">
            <table className="w-full">
              <tr>
                <td className="px-4 py-2">
                  <input
                    type="text"
                    value={mail.Mail}
                    placeholder="Mail"
                    onChange={(e) =>
                      updateData(e, (value) =>
                        setMail((prev) => ({
                          ...prev,
                          Mail: value,
                        }))
                      )
                    }
                    className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                  />
                </td>
                <td className="px-4 py-2">
                  <input
                    type="date"
                    value={mail.Date_1}
                    onChange={(e) =>
                      updateData(e, (value) =>
                        setMail((prev) => ({
                          ...prev,
                          Date_1: value,
                        }))
                      )
                    }
                    className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                  />
                </td>
                <td className="px-4 py-2">
                  <input
                    type="text"
                    value={mail.response}
                    placeholder="Response"
                    onChange={(e) =>
                      updateData(e, (value) =>
                        setMail((prev) => ({
                          ...prev,
                          response: value,
                        }))
                      )
                    }
                    className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                  />
                </td>
                <td className="px-4 py-2">
                  <input
                    type="date"
                    value={mail.Date_2}
                    onChange={(e) =>
                      updateData(e, (value) =>
                        setMail((prev) => ({
                          ...prev,
                          Date_2: value,
                        }))
                      )
                    }
                    className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                  />
                </td>
                <td className="px-4 py-2">
                  <button
                    onClick={addMail}
                    className={`py-1 px-2 rounded bg-blue-500 text-white 
                      }`}
                  >
                    Add
                  </button>
                </td>
              </tr>
            </table>
            {/* give good css to below table it should be scrollable */}
            {patent.mails.length > 0 ? (
              <table className="mail-table w-full">
                <thead>
                  <tr>
                    <th className="border px-4 py-2">Sr. No</th>
                    <th className=" border px-4 py-2">Mail</th>
                    <th className="border px-4 py-2">Mail Sent Date</th>
                    <th className="border px-4 py-2">Response</th>
                    <th className="border px-4 py-2">Response Date</th>
                  </tr>
                </thead>
                <tbody>
                  {patent.mails.map(
                    ({ Mail, Date_1, response, Date_2 }, index) => (
                      <tr key={index}>
                        <td className="border px-4 py-2 text-center">
                          {index + 1}
                        </td>
                        <td className="border px-4 py-2 text-center">
                          <input
                            type="text"
                            value={Mail}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setPatent((prev) => {
                                  const updatedMails = [...prev.mails];
                                  updatedMails[index].Mail = value;
                                  return {
                                    ...prev,
                                    mails: updatedMails,
                                  };
                                })
                              )
                            }
                            className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                          />
                        </td>
                        <td className="border px-4 py-2 text-center">
                          <input
                            type="date"
                            value={Date_1}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setPatent((prev) => {
                                  const updatedMails = [...prev.mails];
                                  updatedMails[index].Date_1 = value;
                                  return {
                                    ...prev,
                                    mails: updatedMails,
                                  };
                                })
                              )
                            }
                            className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                          />
                        </td>
                        <td className="border px-4 py-2 text-center">
                          <input
                            type="text"
                            value={response}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setPatent((prev) => {
                                  const updatedMails = [...prev.mails];
                                  updatedMails[index].response = value;
                                  return {
                                    ...prev,
                                    mails: updatedMails,
                                  };
                                })
                              )
                            }
                            className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                          />
                        </td>
                        <td className="border px-4 py-2 text-center">
                          <input
                            type="date"
                            value={Date_2}
                            onChange={(e) =>
                              updateData(e, (value) =>
                                setPatent((prev) => {
                                  const updatedMails = [...prev.mails];
                                  updatedMails[index].Date_2 = value;
                                  return {
                                    ...prev,
                                    mails: updatedMails,
                                  };
                                })
                              )
                            }
                            className="w-full border border-gray-300 px-2 py-1 rounded focus:outline-none focus:border-blue-500"
                          />
                        </td>
                      </tr>
                    )
                  )}
                </tbody>
              </table>
            ) : (
              <></>
            )}
          </div>
        </div>

        <div className="flex justify-center mt-11">
          <div className="saveButton">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              onClick={handleSave}
            >
              Save
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
export default React.memo(SearchPopup);
