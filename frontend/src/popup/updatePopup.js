import React, { useContext, useEffect, useState } from "react";
import PopupContext from "../context/popupContext.js";
import FormComponenet from "../formComponent.js";
function UpdatePopup() {
  const { company, setCompanyEmpty, setShowUpdate, setShowStage } =
    useContext(PopupContext);
  const updateCompany = async () => {
    try {
      const response = await fetch("http://192.168.1.191:8000/api/updateform", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },

        body: JSON.stringify(company),
      });

      const responseData = await response.json();

      if (response.status === 200) {
        // Data inserted successfully
        alert("Data Saved Successfully");
        setCompanyEmpty();
        setShowUpdate(false);
        setShowStage(true);
      } else if (response.status === 500) {
        alert("company  already exsited");
      } else {
        // Other error cases
        throw new Error("An error occurred while submitting company data");
      }
    } catch (error) {
      // Handle errors
      console.error("Error submitting company data:", error.message);
    }
  };
  return (
    <>
      {" "}
      <FormComponenet></FormComponenet>{" "}
      <div className="flex justify-center mt-11">
        <div className="saveButton">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            onClick={updateCompany}
          >
            Update
          </button>
        </div>
      </div>
    </>
  );
}

export default React.memo(UpdatePopup);
