import React, { useState, useContext, useEffect } from "react";
import PopupContext from "../context/popupContext.js";

function StagePopup() {
  const {
    company,
    setCompany,
    showStage,
    setShowStage,
    companies,
    setCompanies,
    showUpdate,
    setShowUpdate,
    showAddressPopup,
    setShowAddressPopup,
    handleTotalSearchForPage,
  } = useContext(PopupContext);

  const [searchCompany, setSearchCompany] = useState("");
  const [allCompanies, setAllCompanies] = useState([]);
  const [address, setAddress] = useState([]);

  useEffect(() => {
    fetch("http://192.168.1.191:8000/api/onlycompany")
      .then((response) => response.json())
      .then((data) => {
        setAllCompanies(data.data);
      })
      .catch((error) => console.log(error));
  }, []);

  const closeStagePopup = () => {
    setShowStage(false);
  };

  const handleAddressClick = (index, companyIndex) => {
    const addressToFind = companies[companyIndex].addresses[index];
    setAddress(addressToFind);
    setShowAddressPopup(!showAddressPopup);
  };

  const closeAddressClick = () => {
    setShowAddressPopup(!showAddressPopup);
  };

  const handleSearchCompany = () => {
    fetch(`http://192.168.1.191:8000/api/search?search=${searchCompany}`)
      .then((response) => response.json())
      .then((data) => {
        setCompanies(data.data); // Ensure it updates with new data only
      })
      .catch((error) => console.log(error));
  };

  const handleShowUpdate = (companyIndex) => {
    setShowUpdate(true);
    setShowAddressPopup(false);
    setShowStage(false);
    setCompany(companies[companyIndex]);
  };

  const closeSearch = () => {
    setSearchCompany(""); // Clear search input
    handleTotalSearchForPage(); // Reset to initial state or fetch all companies
  };

  return (
    <>
      <div className="bg-white shadow-lg rounded-lg p-6 font-serif tracking-wide w-full mx-4 md:mx-0 relative">
        <div className="relative mb-6">
          <div className="flex justify-between items-center bg-gradient-to-r from-yellow-500 to-yellow-600 p-4 rounded-lg w-full shadow-md">
            <div className="text-3xl font-semibold text-center w-full flex items-center justify-center text-gray-800">
              Search Company {searchCompany && searchCompany}{" "}
              {searchCompany && (
                <button
                  onClick={closeSearch}
                  className="ml-4 text-red-500 hover:text-red-700 transition duration-300 ease-in-out"
                >
                  <i className="fa fa-times-circle" aria-hidden="true"></i>
                </button>
              )}
            </div>
          </div>
        </div>
        <div className="mb-6">
          <input
            type="text"
            value={searchCompany}
            list="searchCompany"
            onChange={(e) => setSearchCompany(e.target.value)}
            placeholder="Enter Company Name"
            className="w-full p-3 border border-gray-300 rounded-lg focus:outline-none focus:ring-2 focus:ring-yellow-400 transition duration-300 ease-in-out"
          />
          <datalist id="searchCompany">
            {allCompanies.map((company) => (
              <option key={company} value={company} />
            ))}
          </datalist>
          <button
            onClick={handleSearchCompany}
            className="mt-4 bg-yellow-500 text-white p-3 rounded-lg hover:bg-yellow-600 transition duration-300 ease-in-out"
          >
            Search Company
          </button>
        </div>
        <div className="overflow-x-auto w-full" style={{ maxHeight: "700px" }}>
          <table className="min-w-full divide-y divide-gray-200 border border-gray-300">
            <thead className="bg-gray-100">
              <tr>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider border border-gray-300">
                  Name of Company
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider border border-gray-300">
                  Company Website
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider border border-gray-300">
                  Category
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider border border-gray-300">
                  Address Details
                </th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-800 uppercase tracking-wider border border-gray-300">
                  Update
                </th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {companies.map((company, companyIndex) => (
                <tr
                  key={company.companyNumber}
                  className="hover:bg-gray-100 transition-colors duration-200"
                >
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900 border border-gray-300">
                    {company.nameOfCompany}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-blue-500 hover:underline border border-gray-300">
                    {company.companyWebsite}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900 border border-gray-300">
                    {company.category}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900 border border-gray-300">
                    {company.addresses.map((address, index) => (
                      <div key={index} className="mb-2">
                        <div
                          className="cursor-pointer text-blue-500 hover:underline"
                          onClick={() =>
                            handleAddressClick(index, companyIndex)
                          }
                        >
                          {address.companyAddress}
                        </div>
                      </div>
                    ))}
                  </td>
                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-900 border border-gray-300">
                    <button
                      onClick={() => handleShowUpdate(companyIndex)}
                      className="bg-yellow-500 text-white p-2 rounded-md hover:bg-yellow-600 transition duration-300 ease-in-out"
                    >
                      Update
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {showAddressPopup && (
          <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
            <div className="bg-white p-6 rounded-lg shadow-lg w-3/4 max-h-full overflow-y-auto">
              <div className="flex justify-between items-center mb-4">
                <h2 className="text-xl font-semibold">Address Details</h2>
                <button
                  className="text-red-500 font-bold"
                  onClick={closeAddressClick}
                >
                  &times;
                </button>
              </div>

              <div className="space-y-4">
                <div>
                  <strong>Address:</strong> {address.companyAddress}
                </div>
                <div>
                  <strong>City:</strong> {address.city}
                </div>
                <div>
                  <strong>State:</strong> {address.state}
                </div>
                <div>
                  <strong>Country:</strong> {address.country}
                </div>
                <div>
                  <strong>Pincode:</strong> {address.pinCode}
                </div>
                <div>
                  <strong>Office Telephone:</strong> {address.officeTelephone}
                </div>
                <div>
                  <strong>Field of Activity:</strong> {address.fieldOfActivity}
                </div>
                <div>
                  <strong>Contacts:</strong>
                  {address.contacts &&
                    address.contacts.map((contact, contactIndex) => (
                      <div key={contactIndex} className="border-t pt-2 mt-2">
                        <div>
                          <strong>Name:</strong> {contact.salutation}{" "}
                          {contact.contactPersonName}
                        </div>
                        <div>
                          <strong>Designation:</strong> {contact.designation}
                        </div>
                        <div>
                          <strong>Department:</strong> {contact.department}
                        </div>
                        <div>
                          <strong>Email:</strong> {contact.email}
                        </div>
                        <div>
                          <strong>Official Mobile:</strong>{" "}
                          {contact.officialMobile}
                        </div>
                        <div>
                          <strong>Personal Mobile:</strong>{" "}
                          {contact.personalMobile}
                        </div>
                        <div>
                          <strong>LinkedIn Profile:</strong>{" "}
                          {contact.linkdinProfile}
                        </div>
                        <div>
                          <strong>Type of Business:</strong>{" "}
                          {contact.typeOfBusiness}
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}

export default React.memo(StagePopup);
