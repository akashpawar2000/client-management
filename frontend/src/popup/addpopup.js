import React, { useContext, useEffect, useState } from "react";
import FormComponenet from "../formComponent.js";
import PopupContext from "../context/popupContext.js";
function AddPopup() {
  const { company, setCompanyEmpty } = useContext(PopupContext);
  const addCompany = async () => {
    try {
      const response = await fetch("http://192.168.1.191:8000/api/addform", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },

        body: JSON.stringify(company),
      });

      const responseData = await response.json();

      if (response.status === 200) {
        // Data inserted successfully
        alert("Data Saved Successfully");
        setCompanyEmpty();
      } else if (response.status === 500) {
        alert("company  already exsited");
      } else {
        // Other error cases
        throw new Error("An error occurred while submitting company data");
      }
    } catch (error) {
      // Handle errors
      console.error("Error submitting company data:", error.message);
    }
  };
  return (
    <>
      <FormComponenet></FormComponenet>
      <div className="flex justify-center mt-11">
        <div className="saveButton">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
            onClick={addCompany}
          >
            Add
          </button>
        </div>
      </div>
    </>
  );
}

export default React.memo(AddPopup);
