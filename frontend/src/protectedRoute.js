import { useContext, useEffect } from "react";
import LoginContext from "./context/loginContext";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import { Route } from "react-router-dom/cjs/react-router-dom.min";
import {
  useHistory,
  useLocation,
} from "react-router-dom/cjs/react-router-dom.min";
function ProtectedRoute({ component: Component, ...rest }) {
  console.log("HII");
  const history = useHistory();
  const { isAuthenticated, setIsAuthenticated } = useContext(LoginContext);
  console.log(isAuthenticated);
  useEffect(() => {
    const unlisten = history.listen((location, action) => {
      if (action === "POP" && location.pathname == "/login") {
        setIsAuthenticated(false);
        localStorage.removeItem("authenticated");
      }
      if (action === "POP" && !isAuthenticated) {
        // Redirect to login page when navigating back if not authenticated
        history.push("/login");
      }
    });

    return () => {
      unlisten();
    };
  }, [history, isAuthenticated]);
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated == true || localStorage.getItem("authenticated") ? (
          <Component {...props}></Component>
        ) : (
          <Redirect to="/login"></Redirect>
        )
      }
    ></Route>
  );
}

export default ProtectedRoute;
